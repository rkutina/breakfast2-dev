library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;
use WORK.BF2_VGA_COMMON.all;

-- THIS MODULE ASSUMES CONTROL OVER ONE COMPLETE BUS.
-- DO NOT CONNECT OTHER DEVICES TO THE SAME BUS, SAVE FOR THE MASTER.

-- This module is designed for 1024x768@60Hz operation.
-- Expected clock is ~65MHz, 64MHz works fine due to the lenient requirements
-- of the VGA standard.

entity bf2_vga_top is
  generic(
    pixel_doubling : integer := 1
    );
  port(
    clk : in std_logic;
    rst : in std_logic;

    io_req  : in  std_logic := '0';
    io_ack  : out std_logic;
    io_wen  : in  std_logic;
    io_ren  : in  std_logic;
    io_adr  : in  std_logic_vector(15 downto 0);
    io_din  : in  std_logic_vector(15 downto 0);
    io_dout : out std_logic_vector(15 downto 0);

    rgb   : out std_logic_vector(7 downto 0);
    hsync : out std_logic;
    vsync : out std_logic;
    blank : out std_logic
    );
end bf2_vga_top;

architecture impl1 of bf2_vga_top is
  -- [+] VGA timing generator
  -- [+] Character generator RAM
  -- [ ] Dual-port character RAM - Background
  -- [ ] Sprite RAM ?
  -- [+] Palette RAM

  type pipe_t is array(0 to 2) of vgapipe_r;
  signal pipe : pipe_t;

  alias endopipe : vgapipe_r is pipe(pipe'length -1);

  signal char_ram_a : std_logic_vector(10 downto 0);
  signal char_ram_d : std_logic_vector(15 downto 0);

  signal vram_a : std_logic_vector(13-2*pixel_doubling downto 0);
  signal vram_d : std_logic_vector(15 downto 0);

  signal font_din  : word16;
  signal font_dout : word16;
  signal font_adr  : word16;
  signal font_wen  : std_logic;

  signal pal_din  : word16;
  signal pal_dout : word16;
  signal pal_adr  : word16;
  signal pal_wen  : std_logic;

  signal vram_din  : word16;
  signal vram_dout : word16;
  signal vram_adr  : word16;
  signal vram_wen  : std_logic;

  type mux_t is (FONT, PAL, VRAM, INVALID);
  signal mux : mux_t := INVALID;

  signal pre_io_dout : word16;

begin

  --- Outer world VGA connections
  rgb   <= endopipe.rgb when endopipe.blank = '0' else (others => '0');
  hsync <= endopipe.hsync;
  vsync <= endopipe.vsync;
  blank <= endopipe.blank;

  --- VGA timing generator
  vga_timing_gen : entity work.bf2_vga_timing(impl1)
    generic map(
      h_vis    => 1024,
      h_fporch => 24,
      h_sync   => 136,
      h_bporch => 160,

      v_vis    => 768,
      v_fporch => 3,
      v_sync   => 6,
      v_bporch => 29
      )
    port map(
      clk   => clk,
      rst   => rst,
      pipe0 => pipe(0)
      );

  --- VGA character generator
  font_ram : entity work.character_ram(impl1)
    port map(
      clk => clk,

      p0_adr  => char_ram_a,
      p0_dout => char_ram_d,

      p1_adr  => font_adr(10 downto 0),
      p1_din  => font_din,
      p1_dout => font_dout,
      p1_wen  => font_wen
      );

  character_gen : entity work.bf2_vga_chargen(impl1)
    generic map(
      pixel_doubling => pixel_doubling
      )
    port map(
      clk => clk,
      rst => rst,

      charmem_d => char_ram_d,
      charmem_a => char_ram_a,

      --- VRAM connections
      vram_d => vram_d,
      vram_a => vram_a,

      --- VGA generation pipeline connections
      pipe_in  => pipe(0),
      pipe_out => pipe(1)
      );

  --- Palette 
  palette : entity work.bf2_vga_palette(impl1)
    port map(
      clk => clk,

      pal_din  => pal_din(7 downto 0),
      pal_adr  => pal_adr(7 downto 0),
      pal_wen  => pal_wen,
      pal_dout => pal_dout(7 downto 0),

      pipe_in  => pipe(1),
      pipe_out => pipe(2)
      );

  --- VRAM
  vram_inst : entity work.RAM_dualport_R_RW(impl1)
    generic map(
      datwidth => 16,
      adrwidth => 14-(2*pixel_doubling),
      size     => (1024/(8*(2**pixel_doubling)) * (768/(8*(2**pixel_doubling))))
      )
    port map(
      clk => clk,

      p0_adr  => vram_a,
      p0_dout => vram_d,

      p1_adr  => vram_adr(13-(2*pixel_doubling) downto 0),
      p1_din  => vram_din,
      p1_dout => vram_dout,
      p1_wen  => vram_wen
      );

  ---
  --- Interface logic
  ---

  --- IO muxes
  with mux select pre_io_dout <=
    pal_dout        when PAL,
    font_dout       when FONT,
    vram_dout       when VRAM,
    (others => '-') when others;
  
  with io_ren select io_dout <=
    pre_io_dout when '1',
    (others => 'Z') when others;
  
  pal_adr  <= io_adr;
  font_adr <= io_adr;
  vram_adr <= io_adr;

  pal_din  <= io_din;
  font_din <= io_din;
  vram_din <= io_din;

  fsm_io :
  process(clk) is
    type state_t is (
      WAIT_REQ,
      IO_INPUT,
      IO_OUTPUT,
      WAIT_ACK
      );

    variable state : state_t := WAIT_ACK;
  begin
    if(rising_edge(clk)) then

      vram_wen <= '0';
      font_wen <= '0';
      pal_wen  <= '0';

      case(state) is
        when WAIT_REQ =>
          if(io_req = '1') then
            state := IO_INPUT;
          end if;

        when IO_INPUT =>
          case io_adr(15 downto 14) is
            when "00" =>
              mux      <= VRAM;
              vram_wen <= io_wen;
            when "01" =>
              mux      <= FONT;
              font_wen <= io_wen;
            when "10" =>
              mux     <= PAL;
              pal_wen <= io_wen;
            when others =>
              mux <= INVALID;
          end case;
          state := IO_OUTPUT;

        when IO_OUTPUT =>
          io_ack <= '1';
          state  := WAIT_ACK;

        when WAIT_ACK =>
          if(io_req = '0') then
            io_ack <= '0';
            state  := WAIT_REQ;
          end if;
      end case;
    end if;
  end process;

end impl1;
