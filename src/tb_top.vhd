library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_top is
end tb_top;

architecture behavior of tb_top is

  -- Component Declaration for the Unit Under Test (UUT)

  component impl_top
    port(
      CLK_32M   : in  std_logic;
      LED       : out std_logic_vector(7 downto 0);
      SWITCH    : in  std_logic_vector(2 downto 0);
      RX        : in  std_logic;
      TX        : out std_logic;
      SEG7_AN   : out std_logic_vector(3 downto 0);
      SEG7_SEG  : out std_logic_vector(7 downto 0);
      VGA_VSYNC : out std_logic;
      VGA_HSYNC : out std_logic;
      VGA_RED   : out std_logic_vector(2 downto 0);
      VGA_GREEN : out std_logic_vector(2 downto 0);
      VGA_BLUE  : out std_logic_vector(1 downto 0)
      );
  end component;


  --Inputs
  signal CLK_32M : std_logic                    := '0';
  signal SWITCH  : std_logic_vector(2 downto 0) := (others => '0');
  signal RX      : std_logic                    := '0';

  --Outputs
  signal LED       : std_logic_vector(7 downto 0);
  signal TX        : std_logic;
  signal SEG7_AN   : std_logic_vector(3 downto 0);
  signal SEG7_SEG  : std_logic_vector(7 downto 0);
  signal VGA_VSYNC : std_logic;
  signal VGA_HSYNC : std_logic;
  signal VGA_RED   : std_logic_vector(2 downto 0);
  signal VGA_GREEN : std_logic_vector(2 downto 0);
  signal VGA_BLUE  : std_logic_vector(1 downto 0);

  -- Clock period definitions
  constant CLK_32M_period : time := 31.25 ns;

begin

  -- Instantiate the Unit Under Test (UUT)
  uut : impl_top port map (
    CLK_32M   => CLK_32M,
    LED       => LED,
    SWITCH    => SWITCH,
    RX        => RX,
    TX        => TX,
    SEG7_AN   => SEG7_AN,
    SEG7_SEG  => SEG7_SEG,
    VGA_VSYNC => VGA_VSYNC,
    VGA_HSYNC => VGA_HSYNC,
    VGA_RED   => VGA_RED,
    VGA_GREEN => VGA_GREEN,
    VGA_BLUE  => VGA_BLUE
    );

  -- Clock process definitions
  CLK_32M_process : process
  begin
    CLK_32M <= '0';
    wait for CLK_32M_period/2;
    CLK_32M <= '1';
    wait for CLK_32M_period/2;
  end process;


  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    SWITCH(0) <= '1';
    wait for 1 us;
    SWITCH(0) <= '0';

    wait for CLK_32M_period*10;

    -- insert stimulus here 

    wait;
  end process;

end;
