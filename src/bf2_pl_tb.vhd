library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.BF2_COMMON.all;

entity bf2_pl_tb is
end bf2_pl_tb;

architecture behavior of bf2_pl_tb is

  -- Component Declaration for the Unit Under Test (UUT)

  component bf2_pipelined is
    port (
      clk     : in  std_logic;
      hold    : in  std_logic;
      rst     : in  std_logic;
      ext_in  : in  std_logic;
      p_adr   : out progctr_t;
      p_din   : in  word16;
      cs_adr  : out word16;
      cs_din  : in  progctr_t;
      cs_dout : out progctr_t;
      cs_wen  : out std_logic;
      io_req  : out std_logic;
      io_ack  : in  std_logic;
      io_ren  : out std_logic;
      io_wen  : out std_logic;
      io_bus  : out std_logic_vector(1 downto 0);
      io_adr  : out word16;
      io_din  : in  word16;
      io_dout : out word16);
  end component bf2_pipelined;

  --Inputs
  signal clk    : std_logic                     := '0';
  signal hold   : std_logic                     := '0';
  signal rst    : std_logic                     := '0';
  signal ext_in : std_logic                     := '0';
  signal p_din  : std_logic_vector(15 downto 0) := (others => '0');
  signal cs_din : progctr_t                     := (others => '0');
  signal io_din : std_logic_vector(15 downto 0) := (others => '0');
  signal io_ack : std_logic                     := '0';

  --Outputs
  signal p_adr   : progctr_t;
  signal cs_adr  : std_logic_vector(15 downto 0);
  signal cs_dout : progctr_t;
  signal cs_wen  : std_logic;
  signal io_ren  : std_logic;
  signal io_wen  : std_logic;
  signal io_req  : std_logic;
  signal io_bus  : std_logic_vector(1 downto 0);
  signal io_adr  : std_logic_vector(15 downto 0);
  signal io_dout : std_logic_vector(15 downto 0);

  -- Clock period definitions
  constant clk_period : time := 10 ns;


  type progmem_t is array(0 to 255) of std_logic_vector(15 downto 0);
  constant progmem : progmem_t := (
    x"F000",
    x"F100",

    x"E154",                            -- MVLI R1 'T'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E168",                            -- MVLI R1 'h'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E169",                            -- MVLI R1 'i'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E173",                            -- MVLI R1 's'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E120",                            -- MVLI R1 ' '
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E177",                            -- MVLI R1 'w'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E16F",                            -- MVLI R1 'o'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E172",                            -- MVLI R1 'r'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E16B",                            -- MVLI R1 'k'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E173",                            -- MVLI R1 's'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E121",                            -- MVLI R1 '!'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E120",                            -- MVLI R1 ' '
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E13A",                            -- MVLI R1 ':'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"E133",                            -- MVLI R1 '3'
    x"3104",                            -- WR0 R1 (R0)
    x"A001",                            -- INC R0

    x"4000",                            -- JMPR 0
    others => (others => '0')
    );

  type callstack_t is array(0 to 255) of progctr_t;
  signal callstack : callstack_t;

begin

  pmem : process(clk) is
  begin
    if(falling_edge(clk))then
      p_din <= progmem(to_integer(unsigned(p_adr(7 downto 0))));
    end if;
  end process;

  csmem : process(clk) is
  begin
    if(falling_edge(clk))then
      if(cs_wen = '1') then
        callstack(to_integer(unsigned(cs_adr(7 downto 0))))
          <= cs_dout;
      end if;

      cs_din <= callstack(to_integer(unsigned(cs_adr(7 downto 0))));
    end if;
  end process;

  -- Instantiate the Unit Under Test (UUT)
  uut : bf2_pipelined port map (
    clk     => clk,
    hold    => hold,
    rst     => rst,
    ext_in  => ext_in,
    p_adr   => p_adr,
    p_din   => p_din,
    cs_adr  => cs_adr,
    cs_din  => cs_din,
    cs_dout => cs_dout,
    cs_wen  => cs_wen,
    io_req  => io_req,
    io_ack  => io_ack,
    io_ren  => io_ren,
    io_wen  => io_wen,
    io_bus  => io_bus,
    io_adr  => io_adr,
    io_din  => io_din,
    io_dout => io_dout
    );

  -- Clock process definitions
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  io_ack <= transport io_req after 53ns;  -- arbitrary value

  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state for 100 ns.
    rst  <= '1';
    hold <= '0';
    wait for 100 ns;
    rst  <= '0';
    hold <= '1';
    wait for 50 ns;
    hold <= '0';

    wait for clk_period*10;

    wait;
  end process;

end;
