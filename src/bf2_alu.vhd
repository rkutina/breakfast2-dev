library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.BF2_COMMON.ALL;

entity bf2_alu is
  generic(
    registered  : std_logic := '1';
    revision    : std_logic_vector(15 downto 0) := X"0000"
  );
  port(
    clk   : in  std_logic := '0';
    ena   : in  std_logic := '1';
    
    ina   : in  std_logic_vector(15 downto 0);
    inb   : in  std_logic_vector(15 downto 0);
    outd  : out std_logic_vector(15 downto 0);
    
    mode  : in  std_logic_vector( 3 downto 0);

    carry : out std_logic;
    zero  : out std_logic
  );
end bf2_alu;

architecture impl1 of bf2_alu is

  subtype word17 is unsigned(16 downto 0);

  signal ina_u        : word17 := (others => '0');
  signal inb_u        : word17 := (others => '0');
  
  signal pre_output   : word17;
  
  signal left_shift   : word17;
  signal right_shift  : word17;

begin

  left_shift  <= ina_u sll to_integer(inb_u);   
  right_shift <= ina_u srl to_integer(inb_u); 
  
  alu_input_reg:
  process(clk, ina, inb) is
  begin
    if((registered = '1' and falling_edge(clk) and ena = '1') or
       (registered = '0')) then
      ina_u <= unsigned('0' & ina);
      inb_u <= unsigned('0' & inb);
    end if;
  end process;
  
  alu_output_reg:
  process(clk, pre_output) is
  begin
    if((registered = '1' and rising_edge(clk) and ena = '1') or 
       (registered = '0')) then
      outd  <= std_logic_vector(pre_output(15 downto 0));
      carry <= pre_output(16);
      if(pre_output(15 downto 0) = "0000000000000000") then
        zero <= '1';
      else
        zero <= '0';
      end if;
    end if;
  end process;
  
  with mode select pre_output <=
   ina_u and inb_u    when m_AND,
   ina_u  or inb_u    when m_IOR,   
   ina_u  +  inb_u    when m_ADD,
   ina_u  -  inb_u    when m_SUB,
   ina_u xor inb_u    when m_XOR,
   inb_u  -  ina_u    when m_SUBR,
   word17'("0" & ina_u(15 downto 8) & inb_u( 7 downto 0))
                      when m_MVL,
   word17'("0" & inb_u( 7 downto 0) & ina_u( 7 downto 0))
                      when m_MVH,
   inb_u              when m_MOV,
   right_shift and "00000000000000001"
                      when m_BSEL, --bitsel
   right_shift        when m_BSR,
   left_shift         when m_BSL,
   word17'("0" & unsigned(revision))
                      when m_REV,
   (others=>'-')      when others;

end impl1;

