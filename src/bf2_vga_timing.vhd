library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.BF2_VGA_COMMON.ALL;

entity bf2_vga_timing is
  generic(
    h_vis     : integer := 1024;
    h_fporch  : integer := 24;
    h_sync    : integer := 136;
    h_bporch  : integer := 160;
    
    v_vis     : integer := 768;
    v_fporch  : integer := 3;
    v_sync    : integer := 6;
    v_bporch  : integer := 29
  );
  port(
    clk   : in  std_logic;
    rst   : in  std_logic;
    pipe0 : out vgapipe_r
  );
end bf2_vga_timing;

architecture impl1 of bf2_vga_timing is

  constant hline : integer := h_vis + h_fporch + h_sync + h_bporch;
  constant vline : integer := v_vis + v_fporch + v_sync + v_bporch;

  signal hcnt : integer := 0;
  signal vcnt : integer := 0;
begin

  
  process( rst, clk ) is
    variable vsync : std_logic;
    variable hsync : std_logic;
    variable blank : std_logic;
    variable framesync : std_logic;
  begin
    if( rst = '1' ) then
      hcnt <= 0;
      vcnt <= 0;
    else
      if( rising_edge(clk) ) then
        framesync := '0';
        if(hcnt < hline-1) then
          hcnt <= hcnt + 1;
        else
          hcnt <= 0;
          
          if(vcnt < vline-1) then
            vcnt <= vcnt + 1;
          else
            vcnt <= 0;
            framesync := '1';
          end if;
        end if;
        
            if (hcnt > (h_vis + h_fporch) and hcnt < (h_vis + h_fporch + h_sync)) then
              hsync := '0';
            else
              hsync := '1';
            end if;
            
            if (vcnt > (v_vis + v_fporch) and vcnt < (v_vis + v_fporch + v_sync)) then
              vsync := '0';
            else
              vsync := '1';
            end if;
            
            if (hcnt > h_vis or vcnt > v_vis) then
              blank := '1';
            else
              blank := '0';
            end if;
            
            pipe0 <= (
              hsync => hsync,
              vsync => vsync,
              blank => blank,
              rgb   => (others => '0'),
              hpix  => hcnt,
              vpix  => vcnt,
              framesync => framesync
            );
      end if;
    end if;
    

  end process;

end impl1;

