library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;

entity bf2_intcon is
  generic(
    adr_ena : word16 := x"00F0"
    );
  port(
    clk : in std_logic;                 -- IO clock
    rst : in std_logic;

    -- CPU connections
    int_ena : in  std_logic;
    int_vec : out std_logic_vector(3 downto 0);
    int_req : out std_logic;

    -- Bus connections
    bus_req  : in  std_logic;
    bus_ack  : out std_logic;
    bus_ren  : in  std_logic;
    bus_wen  : in  std_logic;
    bus_adr  : in  word16;
    bus_din  : in  word16;
    bus_dout : out word16;

    -- Interrupt connections
    i_flag : in  word16;                -- Interrupt flag inputs from devices
    i_ena  : out word16                 -- Interrupt enabled outputs to devices
    );
end entity;

architecture impl1 of bf2_intcon is

  signal flags   : word16;
  signal flagreg : word16;
  signal enables : word16;

begin

  -- Priority encoder:
  int_vec <= x"0" when (flagreg(0) = '1' and enables(0) = '1') else
             x"1" when (flagreg(1) = '1' and enables(1) = '1') else
             x"2" when (flagreg(2) = '1' and enables(2) = '1') else
             x"3" when (flagreg(3) = '1' and enables(3) = '1') else
             x"4" when (flagreg(4) = '1' and enables(4) = '1') else
             x"5" when (flagreg(5) = '1' and enables(5) = '1') else
             x"6" when (flagreg(6) = '1' and enables(6) = '1') else
             x"7" when (flagreg(7) = '1' and enables(7) = '1') else
             x"8" when (flagreg(8) = '1' and enables(8) = '1') else
             x"9" when (flagreg(9) = '1' and enables(9) = '1') else
             x"A" when (flagreg(10) = '1' and enables(10) = '1') else
             x"B" when (flagreg(11) = '1' and enables(11) = '1') else
             x"C" when (flagreg(12) = '1' and enables(12) = '1') else
             x"D" when (flagreg(13) = '1' and enables(13) = '1') else
             x"E" when (flagreg(14) = '1' and enables(14) = '1') else
             x"F";


  magic :
  process(clk, rst) is
    type state_t is (
      INITIAL,
      ENCODE,
      WAIT_ACK
      );
    variable state : state_t := INITIAL;
  begin
    if(rst = '1') then

    elsif(rising_edge(clk)) then
      case(state) is
        when INITIAL =>
          int_req <= '0';

          if(int_ena = '1' and (flags and enables) /= word16'(others => '0')) then
            -- At least one interrupt flag is ready
            flagreg <= flags;
            state   := ENCODE;
          end if;

        when ENCODE =>
          state := WAIT_ACK;

        when WAIT_ACK =>
          int_req <= '1';
          -- Wait for 'ack' from the CPU - the interrupt should get disabled
          -- when they're being processed.

          if(int_ena = '0') then
            -- We're done here, the processor's /probably/ processing our request.
            state := INITIAL;
          end if;
      end case;
    end if;
  end process;

  synchronize_flag_inputs :
  process(clk, rst) is
    type syncpipe_t is array(0 to 1) of word16;
    variable syncpipe : syncpipe_t := (others => (others => '0'));
    variable outflag  : word16;
  begin
    if(rst = '1') then
      outflag     := (others => '0');
      syncpipe(0) := (others => '0');
      syncpipe(1) := (others => '0');
    elsif(rising_edge(clk)) then
      outflag     := syncpipe(1);
      syncpipe(1) := syncpipe(0);
      syncpipe(0) := i_flag;
    end if;

    flags <= outflag;
  end process;

end architecture;
