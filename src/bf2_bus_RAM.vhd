library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;

entity bf2_bus_RAM is
  generic(
    size : integer range 16 to 65536 := 4096
    );
  port(
    clk : in std_logic;
    rst : in std_logic;

    bus_ren  : in  std_logic;
    bus_wen  : in  std_logic;
    bus_req  : in  std_logic;
    bus_ack  : out std_logic;
    bus_adr  : in  word16;
    bus_din  : in  word16;
    bus_dout : out word16
    );
end entity;

architecture impl1 of bf2_bus_RAM is

  signal i_adr   : word16;
  signal i_write : word16;
  signal i_read  : word16;
  signal i_wen   : std_logic;
  signal i_ren   : std_logic;

begin

  RAM : entity work.RAM_generic
    generic map (
      datwidth => 16,
      adrwidth => 16,
      size     => size)
    port map (
      clk     => clk,
      p0_adr  => i_adr,
      p0_din  => i_write,
      p0_dout => i_read,
      p0_wen  => i_wen);

  bus_interface :
  process(clk) is
    type state_t is (
      READY,
      DELAY,
      WAIT_ACK,
      OUTPUT
      );

    variable state : state_t := READY;
  begin
    if(rst = '1') then
      bus_ack  <= '0';
      bus_dout <= (others => 'Z');
      i_wen    <= '0';
      state    := READY;

    elsif(rising_edge(clk)) then
      case(state) is
        when READY =>
          bus_ack  <= '0';
          bus_dout <= (others => 'Z');
          i_wen    <= '0';

          if(bus_req = '1') then
            i_adr   <= bus_adr;
            i_wen   <= bus_wen;
            i_ren   <= bus_ren;
            i_write <= bus_din;

            state := DELAY;
          end if;

        when DELAY =>
          state := OUTPUT;

        when OUTPUT =>
          state := WAIT_ACK;
          if(i_ren = '1') then
            bus_dout <= i_read;
          end if;

        when WAIT_ACK =>
          bus_ack <= '1';

          if(bus_req <= '0') then
            state := READY;
          end if;
      end case;
    end if;
  end process;

end architecture;
