library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;

entity bf2_regfile_0003 is
  port(
    clk : in std_logic;
    rst : in std_logic;

    adrW : in word4;
    adrA : in word4;
    adrB : in word4;

    ren : in std_logic;
    wen : in std_logic;

    datW : in  word16;
    datA : out word16;
    datB : out word16
    );
end entity;

architecture impl1 of bf2_regfile_0003 is

  type regfile_t is array(0 to 15) of word16;
  signal regfile : regfile_t := (others => (others => '-'));

  subtype regindex_t is integer range 0 to 15;

  signal i_adrA : regindex_t;
  signal i_adrB : regindex_t;
  signal i_adrW : regindex_t;

begin

  i_adrA <= to_integer(unsigned(adrA));
  i_adrB <= to_integer(unsigned(adrB));
  i_adrW <= to_integer(unsigned(adrW));

  reg_access :
  process (clk) is
  begin
    if(rising_edge(clk)) then
      if(wen = '1') then
        regfile(i_adrW) <= datW;
      end if;

      if(rst = '1') then
        datA <= (others => '0');
        datB <= (others => '0');
      elsif(ren = '1') then
        if(i_adrA = i_adrW) then
          datA <= datW;
        else
          datA <= regfile(i_adrA);
        end if;

        if(i_adrB = i_adrW) then
          datB <= datW;
        else
          datB <= regfile(i_adrB);
        end if;
      end if;
    end if;
  end process;

end architecture;
