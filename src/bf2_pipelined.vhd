library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;

-- TODO: document code better
--
-- TODO: add JMPREG, add CALLREG
--       ^ not quite that important tbh...
--
-- TODO: add more logic checking accessing 'just written' registers
--
-- DONE: add self-halting functionality
--    ?: add HALT instruction in rev 0004
--
-- TODO: add interrupt support in rev 0004


entity bf2_pipelined is
  port(
    -- Control and clock
    clk  : in std_logic;
    hold : in std_logic;
    rst  : in std_logic;

    ext_in : in std_logic;

    -- Program memory
    p_adr : out progctr_t;
    p_din : in  word16;

    -- Call Stack connections
    cs_adr  : out word16;
    cs_din  : in  progctr_t;
    cs_dout : out progctr_t;
    cs_wen  : out std_logic;

    -- Interrupts
    int_ena : out std_logic;  -- Flag showing that interrupts are enabled.
    int_req : in  std_logic;  -- Interrupt 'request' input. High when int_vec
    -- is stable.
    int_vec : in  std_logic_vector(3 downto 0);

    -- IO bus
    io_req  : out std_logic;
    io_ack  : in  std_logic;
    io_ren  : out std_logic;
    io_wen  : out std_logic;
    io_bus  : out std_logic_vector(1 downto 0);
    io_adr  : out word16;
    io_din  : in  word16;
    io_dout : out word16
    );
end bf2_pipelined;

architecture impl1 of bf2_pipelined is

  constant revision : word16 := X"0003";

  type pipereg0_r is record
    progword : word16;
    progctr  : progctr_t;
    invalid  : std_logic;
  end record;

  type pipereg1_r is record
    progword : word16;
    progctr  : progctr_t;
    invalid  : std_logic;
  end record;

  type pipereg2_r is record
    progword : word16;
    progctr  : progctr_t;
    invalid  : std_logic;
    stackptr : uword16;

    flag_carry : std_logic;
    flag_zero  : std_logic;
    flag_ext   : std_logic;

    datA : word16;
    datB : word16;

    alu_out : word16;
  end record;

  type pipereg3_r is record
    progctr  : progctr_t;
    stackptr : uword16;
  end record;

  -- Pipeline registers
  signal pipe0   : pipereg0_r;
  signal pipe1   : pipereg1_r;
  signal pipe2   : pipereg2_r;
  signal pipe3   : pipereg3_r;
  alias lastpipe : pipereg3_r is pipe3;

  -- ALU wiring
  signal alu_inA   : word16;
  signal alu_inB   : word16;
  signal alu_out   : word16;
  signal alu_carry : std_logic;
  signal alu_zero  : std_logic;
  signal alu_mode  : word4;

  --ALU input and mode mux wiring
  type muxa_t is (MXA_REGA, MXA_PCTR, MXA_IOC, MXA_ZERO);
  type muxb_t is (MXB_REGB, MXB_LIT12, MXB_ZERO, MXB_LIT8);
  type muxm_t is (MXM_LOWBITS, MXM_HIGHBITS, MXM_ADD);
  signal muxa : muxa_t;
  signal muxb : muxb_t;
  signal muxm : muxm_t;

  -- Regfile wiring
  signal reg_adrA      : word4;
  signal reg_adrB      : word4;
  signal reg_adrW      : word4;
  signal reg_ren       : std_logic;
  signal reg_wen       : std_logic;
  signal reg_wen_final : std_logic;
  signal reg_datA      : word16;
  signal reg_datB      : word16;
  signal reg_datW      : word16;

  -- Registered flags
  signal flag_carry : std_logic;
  signal flag_zero  : std_logic;
  signal flag_ext   : std_logic;
  signal f_hold     : std_logic;

  signal f_selfhold : std_logic := '0';  --!0

  -- Pipeline control and misc. signals
  signal new_progctr  : progctr_t;
  signal invalidate   : std_logic;
  signal new_stackptr : uword16;

  -- ALU result feedback
  signal fb_alu_result_data : word16;
  signal fb_alu_canswitchA  : std_logic;
  signal fb_alu_canswitchB  : std_logic;

  -- Pre-multiplexing register data
  --  with possible ALU feedback data
  signal reg_datA_premuxed : word16;
  signal reg_datB_premuxed : word16;

  -- IO Controller wiring
  signal ioc_din  : word16;
  signal ioc_done : std_logic;

  signal int_ena_i : std_logic;
  signal new_int_ena_i : std_logic;

begin

  f_hold  <= hold or rst or f_selfhold or not ioc_done;
  int_ena <= int_ena_i;

  pipe_sync :
  process(clk) is
  
  begin
    if(rising_edge(clk)) then
      if(rst = '1') then
        -- Disable interrupts
        int_ena_i <= '0';

        pipe0.progword <= (others => '0');
        pipe0.progctr  <= (others => '0');
        pipe0.invalid  <= '1';

        pipe1.progword <= (others => '0');
        pipe1.progctr  <= (others => '0');
        pipe1.invalid  <= '1';

        pipe2.progword <= (others => '0');
        pipe2.progctr  <= (others => '1');
        pipe2.invalid  <= '1';

        pipe3.progctr  <= (others => '0');
        pipe3.stackptr <= (others => '0');

      elsif(f_hold = '0') then

        int_ena_i <= new_int_ena_i;

        -- Do the pipeline magic.

        -- Load in new program code
        pipe0.progword <= p_din;
        pipe0.progctr  <= lastpipe.progctr;
        pipe0.invalid  <= invalidate;

        pipe1.progword <= pipe0.progword;
        pipe1.progctr  <= pipe0.progctr;
        pipe1.invalid  <= pipe0.invalid or invalidate;

        pipe2.progword <= pipe1.progword;
        pipe2.progctr  <= pipe1.progctr;
        pipe2.invalid  <= pipe1.invalid or invalidate;

        -- Register new program counter and stack pointer
        pipe3.progctr  <= new_progctr;
        pipe3.stackptr <= new_stackptr;

      -- The 'invalidate' signal is asserted when a jump occurs, invalidating
      -- the previous stages loaded in sequentially 'after' the jump instruction,
      -- thus rendering them invalid indeed.
      --
      -- This 'invalidate' signal should also prevent registers and flags
      -- from being overwritten with invalid data.
      end if;
    end if;
  end process;

  pipe0_combinatorial :
  process(pipe0) is
  begin
    reg_adrA <= pipe0.progword(11 downto 8);
    reg_adrB <= pipe0.progword(7 downto 4);
  end process;

  pipe1_combinatorial :
  process(pipe1, reg_datA, reg_datB) is

  begin
    --- Decode logic
    case pipe1.progword(15 downto 12) is
      when op_ALU =>
        muxa <= MXA_REGA;
        muxb <= MXB_REGB;
        muxm <= MXM_LOWBITS;

      when op_CALL =>
        muxa <= MXA_ZERO;
        muxb <= MXB_LIT12;
        muxm <= MXM_ADD;

      when op_JMP =>
        muxa <= MXA_ZERO;
        muxb <= MXB_LIT12;
        muxm <= MXM_ADD;

      when op_JMPR | op_JMPRE | op_JMPRC | op_JMPRZ =>
        muxa <= MXA_PCTR;
        muxb <= MXB_LIT12;
        muxm <= MXM_ADD;

      when op_EXTRA =>
        case(pipe1.progword(3 downto 0)) is
          when aRD0 | aRD1 | aRD2 | aRD3 =>
            muxa <= MXA_IOC;
            muxb <= MXB_ZERO;
            muxm <= MXM_ADD;

          when aWR0 | aWR1 | aWR2 | aWR3 =>
            muxa <= MXA_REGA;
            muxb <= MXB_ZERO;
            muxm <= MXM_ADD;

          when aRETURN =>
            muxa <= MXA_ZERO;
            muxb <= MXB_ZERO;
            muxm <= MXM_ADD;

          when others =>
            -- Default:
            muxa <= MXA_REGA;
            muxb <= MXB_ZERO;
            muxm <= MXM_ADD;
        end case;

      when op_ANDI | op_IORI | op_ADDI | op_SUBI |
        op_XORI |op_SUBRI | op_MVLI | op_MVHI =>
        muxa <= MXA_REGA;
        muxb <= MXB_LIT8;
        muxm <= MXM_HIGHBITS;

      when others =>
        -- Default:
        muxa <= MXA_REGA;
        muxb <= MXB_ZERO;
        muxm <= MXM_ADD;
    end case;
  end process;

  pipe2_clocked :
  process(clk) is
  begin
    if(rising_edge(clk)) then

      if(f_hold = '0') then

        -- Register ALU flags
        pipe2.flag_carry <= alu_carry;
        pipe2.flag_zero  <= alu_zero;
        pipe2.flag_ext   <= ext_in;
        pipe2.alu_out    <= alu_out;

        pipe2.datA <= reg_datA_premuxed;
        pipe2.datB <= reg_datB_premuxed;

      elsif(rst = '1') then
        pipe2.alu_out <= (others => '0');
      end if;

    end if;
  end process;

  pipe3_writeflags :
  process(clk) is
  begin
    if(rising_edge(clk)) then
      if(invalidate = '0' and pipe2.invalid = '0' and f_hold = '0') then
        flag_carry <= pipe2.flag_carry;
        flag_zero  <= pipe2.flag_zero;
        flag_ext   <= pipe2.flag_ext;
      end if;
    end if;
  end process;

  pipe3_combinatorial_pre_writeback :
  process(pipe2, pipe3, io_din, invalidate) is
    variable write_reg : std_logic;
  begin
    -- Defaults:
    pipe2.stackptr <= lastpipe.stackptr - 1;

    case(pipe2.progword(15 downto 12)) is
      when op_JMP =>
        write_reg := '0';

      when op_JMPR | op_JMPRE | op_JMPRC | op_JMPRZ =>
        write_reg := '0';

      when op_CALL =>
        write_reg := '0';

      when op_EXTRA =>
        case (pipe2.progword(3 downto 0)) is
          when aRETURN =>
            pipe2.stackptr <= lastpipe.stackptr - 1;
            write_reg      := '0';

          when aRD0 | aRD1 | aRD2 | aRD3 =>
            write_reg := '1';
          when others =>
            write_reg := '0';
        end case;

      when others =>
        write_reg := '1';
    end case;

    reg_wen <= write_reg and (not pipe2.invalid) and (not invalidate);

    case(pipe2.progword(15 downto 12)) is
      when op_EXTRA =>
        case(pipe2.progword(3 downto 0)) is
          when aRD0 | aRD1 | aRD2 | aRD3 =>
            reg_datW <= io_din;
          when others =>
            reg_datW <= pipe2.alu_out;
        end case;
      when others =>
        reg_datW <= pipe2.alu_out;
    end case;

    reg_adrW <= pipe2.progword(11 downto 8);  -- Reg A address
  end process;

  pipe3_combinatorial_progctr :
  process(pipe2, pipe3, cs_din, rst, flag_ext, flag_carry, flag_zero) is
    -- Mux conditions around
    -- Generate new progctr
    -- Generate invalidate flag
    -- Assert register write enable
    -- Assert register write data
    variable condition : std_logic;

    alias offset12 : word12 is pipe2.progword(11 downto 0);

    alias old_progctr  : progctr_t is pipe3.progctr;
    alias old_stackptr : uword16 is pipe3.stackptr;
  begin

    -- Condition mux
    case pipe2.progword(15 downto 12) is
      when op_JMPR  => condition := '1';
      when op_JMPRE => condition := flag_ext;
      when op_JMPRC => condition := flag_carry;
      when op_JMPRZ => condition := flag_zero;
      when others   => condition := '-';
    end case;

    -- Defaults:
    cs_wen       <= '0';
    new_stackptr <= old_stackptr;
    cs_dout      <= pipe2.progctr;
    invalidate   <= '0';
    new_int_ena_i <= int_ena_i;
    new_progctr  <= old_progctr + 1;
    
    if(rst = '1') then
      int_ena_i <= '0';
    end if;

    if(int_req = '1' and int_ena_i = '1') then
      -- Handle interrupts
      --
      -- Perform a 'call' to a position in the interrupt vector table.
      new_int_ena_i <= '0';
      new_progctr <= unsigned(x"001" & int_vec);
      invalidate  <= '1';
      cs_dout     <= pipe2.progctr - 1;  -- Should let the 
      cs_wen      <= '1';

    elsif(pipe2.invalid = '0') then
      -- Generate new program counter and invalidate flag
      case pipe2.progword(15 downto 12) is
        when op_CALL =>
          new_progctr  <= resize(unsigned(offset12), 16);
          --new_progctr <= unsigned(pipe2.alu_out);
          invalidate   <= '1';
          -- write the prev prog counter into the jump stack
          new_stackptr <= old_stackptr + 1;
          cs_dout      <= pipe2.progctr;
          cs_wen       <= '1';

        when op_EXTRA =>
          case pipe2.progword(3 downto 0) is

            when aINTDIS =>
              new_int_ena_i    <= '0';
              new_progctr  <= old_progctr + 1;
              new_stackptr <= old_stackptr;
              invalidate   <= '0';
              cs_wen       <= '0';

            when aRETIE =>
              new_int_ena_i    <= '1';
              new_progctr  <= cs_din + 1;
              new_stackptr <= pipe2.stackptr;
              cs_wen       <= '0';
              invalidate   <= '1';

            when aRETURN =>
              new_progctr  <= cs_din + 1;
              new_stackptr <= pipe2.stackptr;
              cs_wen       <= '0';
              invalidate   <= '1';

            when others =>
              new_progctr  <= old_progctr + 1;
              new_stackptr <= old_stackptr;
              invalidate   <= '0';
              cs_wen       <= '0';
          end case;

        when op_JMP =>
          -- Direct jump
          new_progctr <= resize(unsigned(offset12), 16);
          --new_progctr <= unsigned(pipe2.alu_out); -- ALU is used for address calculation to save logic
          invalidate  <= '1';

        when op_JMPR | op_JMPRE | op_JMPRC | op_JMPRZ =>
          -- Conditional relative jumps
          if(condition = '1') then
            -- Add offset to current progctr
            new_progctr <= unsigned(signed(pipe2.progctr) + resize(signed(offset12), 16));
            --new_progctr <= unsigned(pipe2.alu_out); -- ALU is used for address calculation to save logic
            invalidate  <= '1';
          else
            -- Jump not successful, keep going as usual
            new_progctr <= old_progctr + 1;
            invalidate  <= '0';
          end if;

        when others =>
          new_progctr  <= old_progctr + 1;
          new_stackptr <= old_stackptr;
          invalidate   <= '0';
          cs_wen       <= '0';
      end case;
    else
      -- When invalid, assume sequential order
      new_progctr  <= old_progctr + 1;
      new_stackptr <= old_stackptr;
      invalidate   <= '0';
      cs_wen       <= '0';
    end if;
  end process;


  -- Stack pointer output
  cs_adr <= word16(pipe2.stackptr);
  p_adr  <= lastpipe.progctr;

  -- ALU 'A' input Mux
  with muxa select alu_inA <=
    reg_datA_premuxed     when MXA_REGA,
    ioc_din               when MXA_IOC,
    word16(pipe1.progctr) when MXA_PCTR,
    (others => '0')       when MXA_ZERO,
    (others => 'X')       when others;

  -- ALU 'B' input Mux
  with muxb select alu_inB <=
    reg_datB_premuxed                         when MXB_REGB,
    std_logic_vector(resize(signed(pipe1.progword(11 downto 0)), 16))
    when MXB_LIT12,
    ("00000000" & pipe1.progword(7 downto 0)) when MXB_LIT8,
    (others => '0')                           when MXB_ZERO,
    (others => 'X')                           when others;

  -- ALU 'mode' input Mux
  with muxm select alu_mode <=
    ("0" & pipe1.progword(14 downto 12)) when MXM_HIGHBITS,
    pipe1.progword(3 downto 0)           when MXM_LOWBITS,
    m_ADD                                when MXM_ADD,
    m_ADD                                when others;



  -- Mux previous operation's results into the ALU inputs.
  -- This is done in order to keep the ALU inputs 'valid.'
  -- Suppose this code fragment is run:
  --  LDH AG0 0xCA
  --  LDL AG0 0xFE
  -- Due to the pipelined nature of this implementation,
  -- the firt operation would read whatever there is in the register indeed,
  -- and keep going 'as expected.' However, the second instruction, executing
  -- right after the first one, can still only see the 'original' data in the
  -- register, not the data to be written back by the previous instruction
  -- - precisely because it has not yet been written back due to the way
  -- the pipeline operates.
  -- Multiplexing the previous result to the inputs of the next operation can
  -- easily solve this problem, assuming the registers would not change 'by themselves'
  -- after the write. 
  -- This assumption is safe to make in this case, as the registers do not connect to any
  -- external IO, nor contain any constant that might be attempted to be overwritten,
  -- mirrored program counters, etcetera.
  reg_datA_premuxed <= fb_alu_result_data when fb_alu_canswitchA = '1' else reg_datA;
  reg_datB_premuxed <= fb_alu_result_data when fb_alu_canswitchB = '1' else reg_datB;

  fb_alu_result_data <= pipe2.alu_out;

  alu_result_feedback :
  process(pipe1, pipe2) is
    ---
    --- 0: Program register
    --- 1: Read regfile
    --- 2: Register ALU results
    --- 3: Writeback, PC generation
    ---
    --- To generate 'canswitch' flags:
    ---  - check whether the previous operation was indeed an 'ALU' or 'REG+IMMEDIATE' type
    ---  - check whether the register written is any of the ones to be accessed

    -- Opword structure:
    -- 15:12 opword
    -- 11: 8 reg A, also used as a target reg W
    --  7: 4 reg B
    --  3: 0 mode/arg
    alias prev_op      : word4 is pipe2.progword(15 downto 12);
    alias prev_rW      : word4 is pipe2.progword(11 downto 8);
    alias prev_invalid : std_logic is pipe2.invalid;

    alias next_op      : word4 is pipe1.progword(15 downto 12);
    alias next_rA      : word4 is pipe1.progword(11 downto 8);
    alias next_rB      : word4 is pipe1.progword(7 downto 4);
    alias next_invalid : std_logic is pipe1.invalid;

    variable is_eligible_op : std_logic;
  begin

    fb_alu_canswitchA <= '0';
    fb_alu_canswitchB <= '0';

    -- Check whether the op could access any of the written registers
    case(prev_op) is
      when op_ALU =>
        is_eligible_op := '1';
      when op_ANDI | op_IORI | op_SUBI | op_SUBRI |
        op_XORI | op_MVLI | op_MVHI | op_ADDI =>
        is_eligible_op := '1';
      when op_EXTRA =>
        is_eligible_op := '1';
      when others =>
        is_eligible_op := '0';
    end case;

    if(is_eligible_op = '1' and prev_invalid = '0' and next_invalid = '0') then
      -- Check whether the previously written address
      -- matches the one accessed next
      if(prev_rW = next_rA) then
        fb_alu_canswitchA <= '1';
      end if;

      -- Check whether the previously written address
      -- matches the one accessed next
      if(prev_rW = next_rB) then
        fb_alu_canswitchB <= '1';
      end if;
    end if;
  end process;

  --- IO controller
  ioc :
  process(clk) is
    type state_t is (
      INITIAL,
      WAIT_READY,
      OUTPUT_DATA,
      WAIT_ACK,
      FINALIZE
      );
    variable state : state_t;

  begin
    if(rising_edge(clk)) then
      if(rst = '1') then
        state := INITIAL;

        ioc_done <= '1';

        io_wen <= '0';
        io_ren <= '0';
        io_req <= '0';
      else
        case(state) is
          when INITIAL =>
            -- Defaults
            io_wen   <= '0';
            io_ren   <= '0';
            io_req   <= '0';
            ioc_done <= '1';

            case(pipe1.progword(15 downto 12)) is
              when op_EXTRA =>
                case(pipe1.progword(3 downto 0)) is
                  when aRD0 | aRD1 | aRD2 | aRD3 | aWR0 | aWR1 | aWR2 | aWR3 =>
                    ioc_done <= '0';  -- This flag is used to halt the CPU execution when low

                    state := WAIT_READY;
                  when others =>
                    null;
                end case;
              when others =>
                null;
            end case;

          when WAIT_READY =>
            -- Wait for the device to get ready
            io_bus <= pipe2.progword(1 downto 0);  -- Let the bus multiplexor know ahead of time, so the ACK is muxed in correctly
            io_req <= '0';

            if(io_ack = '0') then
              -- When ready, proceed to next stage
              state := OUTPUT_DATA;
            end if;

          when OUTPUT_DATA =>
            -- Register output data in this stage, so they are stable before
            -- the request line is asserted in the following stage.

            -- The data and address are taken from the 'premuxed' inputs to the
            -- ALU. Those already include 'fed-back' values from previous
            -- pipeline stages.
            io_req <= '0';

            io_dout <= pipe2.datA;
            io_adr  <= pipe2.datB;

            io_ren <= not pipe2.progword(2);
            io_wen <= pipe2.progword(2);

            state := WAIT_ACK;

          when WAIT_ACK =>
            -- Assert the request line and wait for device's 'ACK' response.
            io_req <= '1';

            if(io_ack = '1') then
              -- Register data from the IO.
              ioc_din <= io_din;

              -- After response, continue to the next stage.
              state := FINALIZE;
            end if;

          when FINALIZE =>
            io_req <= '0';

            if(io_ack = '0') then
              -- When ready, proceed to next stage
              state    := INITIAL;
              ioc_done <= '1';
            end if;
        end case;
      end if;
    end if;
  end process;

  ---
  --- Entity instantiations
  ---

  alu :
    entity work.bf2_alu(impl1)
      generic map (
        registered => '0',              -- Use external registers
        revision   => revision  -- Value to write using the REVISION instruction
        )
      port map (
        clk => '0',
        ena => '1',

        ina  => alu_inA,
        inb  => alu_inB,
        outd => alu_out,

        mode => alu_mode,

        carry => alu_carry,
        zero  => alu_zero
        );

  reg_wen_final <= reg_wen and not f_hold;
  reg_ren       <= not f_hold;

  regs :
    entity work.bf2_regfile_0003(impl1)
      port map(
        clk => clk,
        rst => rst,

        adrW => reg_adrW,
        adrA => reg_adrA,
        adrB => reg_adrB,

        ren => reg_ren,
        wen => reg_wen_final,

        datW => reg_datW,
        datA => reg_datA,
        datB => reg_datB
        );

end impl1;
