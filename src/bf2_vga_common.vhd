library IEEE;
use IEEE.STD_LOGIC_1164.all;

package bf2_vga_common is

  subtype rgb_t is std_logic_vector(7 downto 0);

  type vgapipe_r is record
      hsync : std_logic;
      vsync : std_logic;
      blank : std_logic;
      rgb   : rgb_t;
      hpix  : integer;
      vpix  : integer;
      framesync : std_logic;
    end record;

end bf2_vga_common;

package body bf2_vga_common is

 
end bf2_vga_common;
