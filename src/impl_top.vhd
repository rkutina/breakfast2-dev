library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;

entity impl_top is
  port(
    CLK_32M : in std_logic;

    LED    : out std_logic_vector(7 downto 0) := (others => '0');
    SWITCH : in  std_logic_vector(2 downto 0);

    RX : in  std_logic;
    TX : out std_logic;

    SEG7_AN  : out std_logic_vector(3 downto 0);
    SEG7_SEG : out std_logic_vector(7 downto 0);

    VGA_VSYNC : out std_logic;
    VGA_HSYNC : out std_logic;
    VGA_RED   : out std_logic_vector(2 downto 0);
    VGA_GREEN : out std_logic_vector(2 downto 0);
    VGA_BLUE  : out std_logic_vector(1 downto 0)
    );
end impl_top;

architecture impl1 of impl_top is

  -- DCM for VGA clock synthesis (64MHz)
  component DCM_VGA
    port(
      CLKIN_IN   : in  std_logic;
      CLKFX_OUT  : out std_logic;
      LOCKED_OUT : out std_logic
      );
  end component;

  -- DCM for CPU clock synthesis (100MHz)
  component DCM_CPU
    port(
      CLKIN_IN   : in  std_logic;
      CLKFX_OUT  : out std_logic;
      LOCKED_OUT : out std_logic
      );
  end component;

  -- DCM for IO clock synthesis (19.2MHz)
  component DCM_IO
    port(
      CLKIN_IN  : in  std_logic;
      CLKFX_OUT : out std_logic
      );
  end component;

  signal CLK_64M : std_logic;
  signal CLK_CPU : std_logic;
  signal CLK_IO  : std_logic;

  signal RST   : std_logic := '1';
  signal BLANK : std_logic;
  signal RGB   : std_logic_vector(7 downto 0);

  signal i_TX : std_logic;
  signal i_RX : std_logic;

  signal bus0_req  : std_logic;
  signal bus0_ack  : std_logic;
  signal bus0_wen  : std_logic;
  signal bus0_ren  : std_logic;
  signal bus0_din  : word16;
  signal bus0_dout : word16;
  signal bus0_adr  : word16;

  signal bus1_req  : std_logic;
  signal bus1_ack  : std_logic;
  signal bus1_wen  : std_logic;
  signal bus1_ren  : std_logic;
  signal bus1_din  : word16;
  signal bus1_dout : word16;
  signal bus1_adr  : word16;

  signal bus2_req  : std_logic;
  signal bus2_ack  : std_logic;
  signal bus2_wen  : std_logic;
  signal bus2_ren  : std_logic;
  signal bus2_din  : word16;
  signal bus2_dout : word16;
  signal bus2_adr  : word16;

  signal bus3_req  : std_logic;
  signal bus3_ack  : std_logic;
  signal bus3_wen  : std_logic;
  signal bus3_ren  : std_logic;
  signal bus3_din  : word16;
  signal bus3_dout : word16;
  signal bus3_adr  : word16;

  --
  signal cpu_hold : std_logic := '0';
  signal cpu_ext  : std_logic;

  --
  signal ioc_wen  : std_logic;
  signal ioc_ren  : std_logic;
  signal ioc_req  : std_logic;
  signal ioc_ack  : std_logic;
  signal ioc_adr  : word16;
  signal ioc_din  : word16;
  signal ioc_dout : word16;
  signal ioc_bus  : std_logic_vector(1 downto 0);

  -- Progmem wiring
  signal p_adr : progctr_t;
  signal p_dat : word16 := (others => '0');

  -- IntCon wiring
  signal int_ena : std_logic;
  signal int_req : std_logic := '0';
  signal int_vec : std_logic_vector(3 downto 0);

  -- Call Stack wiring
  signal cs_din    : progctr_t;
  signal cs_dout   : progctr_t;
  signal cs_adr    : word16;
  signal cs_wen    : std_logic;
  signal cs_din_pc : word16;

  signal display : word16;

begin

  TX   <= i_TX;
  i_RX <= RX;

  --LED(5 downto 0) <= std_logic_vector(p_adr(5 downto 0));
  LED(7) <= i_TX;
  LED(6) <= RST;
  LED(5) <= ioc_bus(1);
  LED(4) <= ioc_bus(0);
  LED(3) <= bus0_wen;
  LED(2) <= bus0_ren;
  LED(1) <= bus0_ack;
  LED(0) <= bus0_req;

  -- Power On Reset circuit
  -- ----------------------
  -- Returns the system to a known state after power-up.
  -- It also has an external reset input - the POR circuit then makes sure the
  -- reset is actually held for a specified minimum amount of time.
  por :
    entity work.por_circuit
      port map(
        clk     => CLK_64M,
        rst_in  => SWITCH(0),
        rst_out => RST
        );

  -- VGA/64MHz clock synthesizer
  -- ---------------------------
  -- Used to produce the 64MHz clock.
  -- This clock is then used for most of the system, including VGA sync generation.
  -- Xilinx specific.
  Inst_DCM_VGA : DCM_VGA port map(
    CLKIN_IN  => CLK_32M,
    CLKFX_OUT => CLK_64M
    );

  -- CPU/100MHz clock synthesizer
  -- ----------------------------
  -- Used to produce the CPU clock, here chosen to be 100MHz.
  -- Xilinx specific.
  Inst_DCM_CPU : DCM_CPU port map(
    CLKIN_IN  => CLK_64M,
    CLKFX_OUT => CLK_CPU
    );

  -- IO/38.4MHz clock synthesizer
  -- ----------------------------
  -- Used to produce the 38.4MHz clock used for UART and other IO.
  -- Xilinx specific.
  Inst_DCM_IO : DCM_IO port map(
    CLKIN_IN  => CLK_64M,
    CLKFX_OUT => CLK_IO
    );

  -- Call Stack memory
  -- -----------------
  -- Holds the program addresses to return to
  cs_mem : entity work.RAM_generic(impl1)
    generic map(
      datwidth => 16,
      adrwidth => 16,
      size     => 64                    -- call stack depth
      )
    port map(
      clk => CLK_CPU,

      p0_adr  => cs_adr,
      p0_din  => word16(cs_dout),
      p0_wen  => cs_wen,
      p0_dout => cs_din_pc
      );
  cs_din <= progctr_t(cs_din);

  -- Breakfast Mk. 2 CPU itself
  -- --------------------------
  -- Does the magic.
  cpu : entity work.bf2_pipelined
    port map(
      -- Control and clock
      clk  => CLK_CPU,
      hold => cpu_hold,
      rst  => RST,

      ext_in => '0',
      -- Program memory
      p_adr  => p_adr,
      p_din  => p_dat,

      -- Call Stack connections
      cs_adr  => cs_adr,
      cs_din  => unsigned(cs_din),
      cs_dout => cs_dout,
      cs_wen  => cs_wen,

      -- Interrupts
      int_ena => int_ena,
      int_req => int_req,
      int_vec => int_vec,

      -- IO bus
      io_req  => ioc_req,
      io_ack  => ioc_ack,
      io_ren  => ioc_ren,
      io_wen  => ioc_wen,
      io_bus  => ioc_bus,
      io_adr  => ioc_adr,
      io_din  => ioc_din,
      io_dout => ioc_dout
      );

  -- Bus mux.
  -- --------
  -- A large multiplexor enabling the use of four different buses, as supported
  -- by the BF2 instruction set.
  busmux : entity work.bf2_busmux
    generic map (
      bus0_cpusynchronous => '0',
      bus1_cpusynchronous => '0',
      bus2_cpusynchronous => '1',
      bus3_cpusynchronous => '1'
      )
    port map (
      cpu_clk  => CLK_CPU,
      cpu_bus  => ioc_bus,
      cpu_req  => ioc_req,
      cpu_ack  => ioc_ack,
      cpu_din  => ioc_din,
      cpu_dout => ioc_dout,
      cpu_adr  => ioc_adr,
      cpu_wen  => ioc_wen,
      cpu_ren  => ioc_ren,

      -- IO bus, used for the UART
      bus0_clk  => CLK_IO,
      bus0_req  => bus0_req,
      bus0_ack  => bus0_ack,
      bus0_din  => bus0_din,
      bus0_dout => bus0_dout,
      bus0_adr  => bus0_adr,
      bus0_wen  => bus0_wen,
      bus0_ren  => bus0_ren,

      -- Video bus, used to talk to the VGA generator
      bus1_clk  => CLK_64M,
      bus1_req  => bus1_req,
      bus1_ack  => bus1_ack,
      bus1_din  => bus1_din,
      bus1_dout => bus1_dout,
      bus1_adr  => bus1_adr,
      bus1_wen  => bus1_wen,
      bus1_ren  => bus1_ren,

      -- BUS 2 used for a data RAM
      bus2_clk  => CLK_CPU,
      bus2_req  => bus2_req,
      bus2_ack  => bus2_ack,
      bus2_din  => bus2_din,
      bus2_dout => bus2_dout,
      bus2_adr  => bus2_adr,
      bus2_wen  => bus2_wen,
      bus2_ren  => bus2_ren,

      -- BUS 3 used for progmem connections
      bus3_clk  => CLK_CPU,
      bus3_req  => bus3_req,
      bus3_ack  => bus3_ack,
      bus3_din  => bus3_din,
      bus3_dout => bus3_dout,
      bus3_adr  => bus3_adr,
      bus3_wen  => bus3_wen,
      bus3_ren  => bus3_ren
      );

  -- 'Data' RAM
  -- ----------
  -- General purpose memory to store non-executable data.
  -- 
  data_RAM : entity work.bf2_bus_RAM
    generic map (
      size => 4096
      )
    port map (
      clk      => CLK_CPU,
      rst      => RST,
      bus_ren  => bus2_ren,
      bus_wen  => bus2_wen,
      bus_req  => bus2_req,
      bus_ack  => bus2_ack,
      bus_adr  => bus2_adr,
      bus_din  => bus2_dout,
      bus_dout => bus2_din
      );

  -- Program memory
  -- --------------
  -- Holds the program executed by the CPU itself
  -- It is a RAM, not ROM as such. The bus connection then allows rewriting the
  -- program while the CPU operates. This enables use of simple bootloaders and
  -- similar abominations.
  cpu_pmem : entity work.bf2_progmem
    generic map(
      size => 4096
      )
    port map(
      clk => CLK_CPU,
      rst => RST,

      -- CPU connections
      p_adr  => p_adr,
      p_dout => p_dat,

      -- BUS connections
      bus_ren  => bus3_ren,
      bus_wen  => bus3_wen,
      bus_req  => bus3_req,
      bus_ack  => bus3_ack,
      bus_adr  => bus3_adr,
      bus_din  => bus3_dout,
      bus_dout => bus3_din
      );

  -- VGA generator
  -- -------------
  -- Does all the magic required to get a picture on your VGA screen.
  vga : entity work.bf2_vga_top(impl1)
    generic map(
      pixel_doubling => 1
      )
    port map(
      clk => CLK_64M,
      rst => RST,

      io_req  => bus1_req,
      io_ack  => bus1_ack,
      io_wen  => bus1_wen,
      io_ren  => bus1_ren,
      io_adr  => bus1_adr,
      io_din  => bus1_dout,
      io_dout => bus1_din,

      rgb   => RGB,
      hsync => VGA_HSYNC,
      vsync => VGA_VSYNC,
      blank => BLANK
      );

  -- UART - Asynchronous Receiver/Transmitter
  -- ----------------------------------------
  -- A cut-down simplistic UART.
  -- Contains a 16 character buffer, and only runs at one baudrate for now. Simple.
  -- Can be configured for both bus blocking and non-blocking operation at
  -- synth time.
  UART : entity work.uart_simple
    generic map(
      adr_reg_rx => x"0101",
      adr_reg_tx => x"0102",

      divide => 250  -- Assuming 38.4MHz clock, this will result in 9600 Bd operation -- this divides the Baud x 16 clock even further
     -- The resulting baudrate is CLK / (16*divide).
      )
    port map(
      clk_16baud => CLK_IO,
      rst        => RST,

      rx => i_RX,
      tx => i_TX,

      io_req  => bus0_req,
      io_ack  => bus0_ack,
      io_wen  => bus0_wen,
      io_ren  => bus0_ren,
      io_adr  => bus0_adr,
      io_din  => bus0_dout,
      io_dout => bus0_din,

      rx_dataready => open,
      tx_ready     => cpu_ext
      );

  VGA_RED (2 downto 0)   <= RGB(7 downto 5) when blank = '0' else (others => '0');
  VGA_GREEN (2 downto 0) <= RGB(4 downto 2) when blank = '0' else (others => '0');
  VGA_BLUE (1 downto 0)  <= RGB(1 downto 0) when blank = '0' else (others => '0');

  -- Seven segment display driver
  -- ----------------------------
  -- A genericized sevensegment display driver, able to drive multiple digits
  -- at once using multiplexing. Displays the word input to it as a series of
  -- hexadecimal digits.
  sevenseg :
    entity work.sevenseg_driver
      generic map(
        ndigits => 4
        )
      port map(
        clk => CLK_CPU,
        rst => RST,

        data_in => display,

        commons  => SEG7_AN,
        segments => SEG7_SEG(6 downto 0)
        );
  SEG7_SEG(7) <= '1';  -- Segments and anodes are active low. SEG7_SEG(7)
  -- corresponds to a decimal point, which should be currently unused.

  -- Select what to display on the seven segment thing
  with SWITCH(2 downto 1) select display <=
    word16(p_adr)   when "00",          -- Currently fetched program address
    p_dat           when "01",          -- Currently fetched program data
    ioc_adr         when "10",          -- IO address
    ioc_dout        when "11",          -- IO data
    (others => '0') when others;

end impl1;
