LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY bf2_tb IS
END bf2_tb;
 
ARCHITECTURE behavior OF bf2_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bf2_datapath
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         ena : IN  std_logic;
         p_adr : OUT  std_logic_vector(15 downto 0);
         p_din : IN  std_logic_vector(15 downto 0);
         ext_in : IN  std_logic;
         io_req : OUT  std_logic;
         io_ack : IN  std_logic;
         io_rd : OUT  std_logic;
         io_wr : OUT  std_logic;
         io_bus : OUT  std_logic_vector(2 downto 0);
         io_adr : OUT  std_logic_vector(15 downto 0);
         io_din : IN  std_logic_vector(15 downto 0);
         io_dout : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal ena : std_logic := '0';
   signal p_din : std_logic_vector(15 downto 0) := (others => '0');
   signal ext_in : std_logic := '0';
   signal io_ack : std_logic := '0';
   signal io_din : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal p_adr : std_logic_vector(15 downto 0);
   signal io_req : std_logic;
   signal io_rd : std_logic;
   signal io_wr : std_logic;
   signal io_bus : std_logic_vector(2 downto 0);
   signal io_adr : std_logic_vector(15 downto 0);
   signal io_dout : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
  subtype word16 is std_logic_vector(15 downto 0);
  type pmem_t is array(0 to 65535) of word16;
  
  constant pmem : pmem_t := (
  
--             -- ; Fill up internal memory:
--    X"0ECA", -- MOV OFFA ZERO
--             -- loop:
--    X"00EA", -- MOV AG0 OFFA
--    X"AE01", -- ADDI OFFA 1
--    X"4FFE", -- JMPR -2
    
              -- ; Fibonacci
    X"0ECA",  -- MOV  OFFA  ZERO  : 0
    X"00CA",  -- MOV  AG0   ZERO  : 1
    X"A001",  -- ADDI AG0   1     : 2
    X"01CA",  -- MOV  AG1   ZERO  : 3
    X"A102",  -- ADDI AG1   1     : 4
    X"020A",  -- MOV  AG2   AG0   : 5
    X"0212",  -- ADD  AG2   AG1   : 6
    X"7003",  -- JMPRC +3         : 7
    X"AE01",  -- ADDI OFFA  1     : 8
    X"1005",  -- JMP   5          : 9
    X"4000",  -- JMPR  0          : 10

--      X"0000",  --
--      X"4F00",  -- JMPR 0x030

--    X"00CA",  --MOV   AG0   ZERO
--    X"E0FF",  --MOVIL AG0   0xFF
--    X"F0FF",  --MOVIH AG0   0xFF
--    X"A001",  --ADDI  AG0   0x01
--    X"7003",  --JMPRC 3
--    X"4005",  --JMPR  5
--    X"1FFF",  --JMP   0xFFF
--    X"A101",  --ADDI  AG1   1
--    X"4000",  --JMPR  0
--    X"A102",  --ADDI  AG1   2
--    X"4000",  --JMPR  0
--    X"A104",  --ADDI  AG1   4
--    X"4000",  --JMPR  0
--    X"1AAA",  --JMP   0xAAA
    
 
    others => X"0000"
  );
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bf2_datapath PORT MAP (
          clk => clk,
          rst => rst,
          ena => ena,
          p_adr => p_adr,
          p_din => p_din,
          ext_in => ext_in,
          io_req => io_req,
          io_ack => io_ack,
          io_rd => io_rd,
          io_wr => io_wr,
          io_bus => io_bus,
          io_adr => io_adr,
          io_din => io_din,
          io_dout => io_dout
        );
    
   prom:
   process ( clk ) is
   begin
     if ( rising_edge(clk) ) then
       p_din <= pmem(to_integer(unsigned(p_adr)));
     end if;
   end process;

   io_ack <= transport io_req after clk_period*50;

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      rst <= '1';
      ena <= '1';
      wait for 100 ns;	
      rst <= '0';

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;
   
   

END;
