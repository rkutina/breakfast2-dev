library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity synchronizer is
  generic(
    thru : std_logic := '0'  -- When this is '1', the synchronizer simply
   -- lets all inputs to the outputs, effectively
   -- disabling it.
    );
  port(
    src_clk : in  std_logic := '0';
    tgt_clk : in  std_logic;
    req_in  : in  std_logic;
    req_out : out std_logic
    );
end synchronizer;

architecture impl1 of synchronizer is
  signal intermediate : std_logic;
begin

  process(tgt_clk, req_in) is
  begin
    if(thru = '0') then
      if(rising_edge(tgt_clk)) then
        req_out      <= intermediate;
        intermediate <= req_in;
      end if;
    elsif(thru = '1') then
      req_out <= req_in;
    end if;
  end process;

end impl1;

architecture impl2 of synchronizer is
  signal thing : std_logic;

  signal intermediate : std_logic;
begin

  process(src_clk, req_in) is
  begin
    if(thru = '0') then
      if(rising_edge(src_clk)) then
        thing <= req_in;
      end if;
    elsif(thru = '1') then
      thing <= req_in;
    end if;
  end process;

  process(tgt_clk, thing) is
  begin
    if(thru = '0')then
      if(rising_edge(tgt_clk)) then
        req_out      <= intermediate;
        intermediate <= thing;
      end if;
    elsif(thru = '1') then
      req_out <= thing;
    end if;
  end process;

end impl2;
