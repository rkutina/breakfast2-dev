library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity por_circuit is
  generic(
    ncycles : integer := 16000
  );
  port(
    clk : in std_logic;
    rst_in : in std_logic;
    rst_out : out std_logic := '1'
  );
end por_circuit;

architecture impl1 of por_circuit is
  signal waitcounter : integer range 0 to ncycles := 0;
begin

  process(clk) is
    
  begin
    if(rising_edge(clk))then
      if(waitcounter < ncycles) then
        rst_out <= '1';
        waitcounter <= waitcounter + 1;
      elsif(rst_in = '1') then
        rst_out <= '1';
        waitcounter <= 0;
      else
        rst_out <= '0';
      end if;
    end if; 
  end process;

end impl1;

