library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.BF2_COMMON.ALL;

entity bf2_datapath is
  generic(
    iram_size : integer range 4 to 256 := 64
  );
  port(
    clk : in std_logic;
    rst : in std_logic;
    ena : in std_logic;
    
    -- Program bus
    p_adr   : out word16;
    p_din   : in  word16;
    
    -- Ext. condition
    ext_in  : in  std_logic;
    
    -- IO bus
    io_req  : out std_logic;
    io_ack  : in  std_logic;
    io_rd   : out std_logic;
    io_wr   : out std_logic;
    
    io_bus  : out std_logic_vector( 2 downto 0);
    
    io_adr  : out word16;
    io_din  : in  word16;
    io_dout : out word16
  );
end bf2_datapath;

architecture impl1 of bf2_datapath is
  
  ---
  --- Register File wiring
  ---
  
  signal  r_pc      : word16;
  signal  pc_n      : word16;
  
  signal reg_adrA   : word4;
  signal reg_adrB   : word4;
  signal reg_adrW   : word4;
  
  signal reg_wen    : std_logic;
  signal reg_ren    : std_logic;
  signal reg_wpc    : std_logic;
  
  signal reg_datA   : word16;
  signal reg_datB   : word16;
  signal reg_datW   : word16;
  
  ---
  --- ALU wiring
  --- 
  signal alu_inA    : word16;
  signal alu_inB    : word16;
  signal alu_out    : word16;
  signal alu_carry  : std_logic;
  signal alu_zero   : std_logic;
  
  signal last_alu_carry : std_logic;
  signal last_alu_zero  : std_logic;
  
  signal alu_mode   : word4;
  
  ---
  --- MUXes
  ---
  
  type sel_inA_t is (
    REG,
    PC,
    ZERO,
    IO_IN
  );
  signal sel_inA : sel_inA_t;
  
  type sel_inB_t is (
    REG,
    IO_IN,
    LIT12,
    LIT8,
    ZERO
  );
  signal sel_inB : sel_inB_t;
  
  type sel_alumode_t is (
    PROG,
    ADD,
    UPPER
  );
  signal sel_alumode : sel_alumode_t;
  
  type sel_pcn_t is (
    INC,
    ALU,
    LOAD12
  );
  signal sel_pcn : sel_pcn_t;
  
  
  signal sel_cond : std_logic_vector(1 downto 0);
  signal condition : std_logic;
  
  ---
  --- Progword and aliases
  ---

  signal progword   : word16;
  
  alias  prog_adrA  : word4  is progword(11 downto  8);
  alias  prog_adrB  : word4  is progword( 7 downto  4);
  alias  prog_op    : word4  is progword(15 downto 12);
  alias  prog_mode  : word4  is progword( 3 downto  0);
  alias  prog_lit12 : word12 is progword(11 downto  0);
  alias  prog_rw    : std_logic is progword(3);
  alias  prog_busno : std_logic_vector(2 downto 0) is progword(2 downto 0);

  
  constant zero16 : word16 := (others => '0');

begin
  
  reg_ren <= '1';
  
  io_dout <= reg_datA;
  io_adr  <= reg_datB;
  
  p_adr   <= r_pc;
  
  reg_datW <= alu_out;
  
  reg_adrW <= prog_adrA;
  reg_adrA <= prog_adrA;
  reg_adrB <= prog_adrB;
  
  --- 
  --- MUXes
  ---
  with sel_inA select alu_inA <= 
    reg_datA      when REG,
    r_pc          when PC,
    (others=>'0') when ZERO,
    
    reg_datA      when others;
             
  with sel_inB select alu_inB <=             
    reg_datB      when REG,
    io_din        when IO_IN,
    word16(resize(signed(prog_lit12),16))
                  when LIT12,

    word16(resize(unsigned(prog_lit12(7 downto 0)),16))
                  when LIT8,
    (others=>'0') when ZERO,
    
    reg_datB      when others;
  
  with sel_alumode select alu_mode <=
    prog_mode     when PROG,
    ('0' & prog_op(2 downto 0))
                  when UPPER,
    "0010"        when ADD;
    
  with sel_cond select condition <= 
    '1'            when "00",
    ext_in         when "01",
    last_alu_zero  when "10",
    last_alu_carry when "11",
    '0' when others;
    
  ---
  --- PC new value mux
  ---
  with sel_pcn select pc_n <=
    word16(unsigned(r_pc) + 1) when INC,
    alu_out                    when ALU,
    word16("0000"& prog_lit12) when LOAD12,
    (others=>'1')              when others;
    
  --- 
  --- Registers
  ---
  regfile:
  entity work.bf2_regfile(impl1)
	generic map(
		iram_size => iram_size
	)
	port map(
		clk     => clk,
		rst     => rst,
    ena     => ena,
		
		adrW    => reg_adrW,
		adrA    => reg_adrA,
		adrB    => reg_adrB,
		
		ren     => reg_ren,
    wen     => reg_wen,
    wpc     => reg_wpc,
		
		datW    => reg_datW,
		datA    => reg_datA,
		datB    => reg_datB,
		
		pc_in   => pc_n,
		pc_out 	=> r_pc
	);
  
  ---
  --- ALU
  ---
  alu_inst:
  entity work.bf2_alu(impl1) port map(
    clk   => clk,
    ena   => ena,
  
    ina   => alu_inA,
    inb   => alu_inB,
    outd  => alu_out,
    
    mode  => alu_mode,
    
    carry => alu_carry,
    zero  => alu_zero
  );
  
  
  
  ---
  --- Decoding logic
  ---
  decoder:
  process( clk, ena, rst ) is
    type state_t is (
      INIT,
      LATCH,
      DECODE,
      WAIT_ACK,
      WRITEBACK,
      DUMMY
    );
    
    variable state : state_t := INIT;
  begin
  
    if(ena = '1') then
      if(rst = '1') then
        state := INIT;
      elsif( rising_edge(clk)) then
        
        case state is
          when INIT =>
            reg_wen <= '0';
            io_rd   <= '0';
            io_wr   <= '0';
            io_req  <= '0';
            reg_wpc <= '0';
            
            state := LATCH;
            
          when LATCH => 
            reg_wpc <= '0';
            reg_wen <= '0';
            progword <= p_din;
            state := DECODE;
            
          when DECODE =>
            reg_wpc <= '0';
            reg_wen <= '0';
            state := WRITEBACK;
            
            -- defaults
            sel_inA <= REG;
            sel_inB <= REG;
            sel_alumode <= PROG;
            
            case( prog_op ) is
              when "0000" =>
                --- 0000 aaaa bbbb mmmm ALU operation
                --- A reg sourceA, target
                --- B reg sourceB
                --- M alu mode
                sel_inA <= REG;
                sel_inB <= REG;
                sel_alumode <= PROG;
              
              when "0001" =>
                --- 0001 jjjj jjjj jjjj JMP
                sel_inA <= ZERO;
                sel_inB <= LIT12;
                sel_alumode <= ADD;
              
              when "0010" => 
                --- 0010 aaaa bbbb 0nnn IO  RD a n.(b)
                --- 0010 aaaa bbbb 1nnn IO  WR a n.(b)
                --- n is a bus number
                io_rd   <= '0';
                io_wr   <= '0';
                io_bus  <= prog_busno;
                io_req  <= '0';
                
                if ( prog_rw = '1' ) then
                  -- IO write
                  io_wr <= '1';
                  sel_inA <= REG;
                  sel_inB <= ZERO;
                else
                  -- IO read
                  io_rd <= '1';
                  sel_inA <= ZERO;
                  sel_inB <= IO_IN;
                end if;
                
                if( io_ack = '1' ) then
                  -- wait for IO to go down with ack
                  state := DECODE;
                else
                  state := WAIT_ACK;
                end if;
              
              when "0011" =>
                --- Unused
                --assert false report "Unimplemented instruction";
                null;
              
              when "0100" | "0101" | "0110" | "0111" =>
                -- 0100 jjjj jjjj jjjj JMPR
                -- 0101 jjjj jjjj jjjj JMPRE	--external
                -- 0110 jjjj jjjj jjjj JMPRZ	--zero
                -- 0111 jjjj jjjj jjjj JMPRC	--carry
                
                sel_inA     <= PC;
                sel_inB     <= LIT12;
                sel_alumode <= ADD;
                sel_cond    <= prog_op(1 downto 0);
              
              
              when "1000" | "1001" | "1010" | "1011" |
                   "1100" | "1101" | "1110" | "1111" =>
                sel_inA     <= REG;
                sel_inB     <= LIT8;
                sel_alumode <= UPPER;
                   
              when others =>
                --assert false report "Unrecognized instruction";
                null;
            end case;
            
          when WAIT_ACK =>
            io_req <= '1';
            
            if( io_ack = '1' ) then
              state := WRITEBACK;
            else
              -- wait for IO to ack
            end if;
          
          when WRITEBACK =>
            state := DUMMY;
            
            io_req <= '0';
            
            sel_pcn <= INC;
            reg_wpc <= '1';
            reg_wen <= '1';
            
            if( prog_op = "0000" or prog_op(3) = '1' ) then
              assert false report "boop!";
              last_alu_carry <= alu_carry;
              last_alu_zero  <= alu_zero;
            end if;
            
            case prog_op is
            
              when "0001" =>
                --- 0001 jjjj jjjj jjjj JMP
                sel_pcn <= LOAD12;
                reg_wen <= '0';
                
              when "0100" | "0101" | "0110" | "0111" =>
                -- 0100 jjjj jjjj jjjj JMPR
                -- 0101 jjjj jjjj jjjj JMPRE	--external
                -- 0110 jjjj jjjj jjjj JMPRZ	--zero
                -- 0111 jjjj jjjj jjjj JMPRC	--carry
                if( condition = '1' ) then
                  sel_pcn <= ALU;
                end if;
                reg_wen <= '0';
                
              when others => null;
            end case;
          when DUMMY =>
            reg_wpc <= '0';
            reg_wen <= '0';
            
            state := LATCH;
            
          when others =>
            --assert false report "Reverting to initial state";
            state := INIT;
            null;
        end case;
        
      end if;
    end if;
  
  end process;
  
  -- statement: VHDL is begin stupid; end VHDL;


end impl1;