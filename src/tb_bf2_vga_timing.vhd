  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;
  use WORK.BF2_VGA_COMMON.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

  -- Component Declaration
          COMPONENT bf2_vga_timing
          port(
            clk   : in  std_logic;
            rst   : in  std_logic;
            pipe0 : out vgapipe_r
          );
          END COMPONENT;

          SIGNAL clk :  std_logic;
          SIGNAL rst :  std_logic;
          SIGNAL pipe0 : vgapipe_r;
          

          constant clk_period : time := 10 ns;
  BEGIN

  -- Component Instantiation
          uut: bf2_vga_timing PORT MAP(
            clk => clk,
            rst => rst,
            pipe0 => pipe0
          );


  --  Test Bench Statements
     clk_process :process
     begin
      clk <= '0';
      wait for clk_period/2;
      clk <= '1';
      wait for clk_period/2;
     end process;
   
     tb : PROCESS
     BEGIN
        rst <= '1';
        wait for 100 ns;
        rst <= '0';
        
        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
