library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.BF2_COMMON.ALL;

entity bf2_io_ctrl is
    generic(
        busno       : std_logic_vector(1 downto 0) := "00"
        );
    port(
        -- CPU domain connections
        cpu_clk  : in std_logic;
        cpu_rst  : in std_logic;        -- Reset coming from the CPU domain
        cpu_hold : out std_logic := '0';
        cpu_dout : out word16;
        cpu_din  : in word16;
        cpu_adr  : in word16;
        cpu_bus  : in std_logic_vector(1 downto 0);     -- Bus selection
        cpu_rd   : in std_logic;
        cpu_wr   : in std_logic;

        -- (A)synchronous IO connections
        io_clk  : in std_logic;
        io_rst  : out std_logic;        -- Reset synchronized to the IO domain
        io_din  : in word16;
        io_dout : out word16;
        io_adr  : out word16;
        io_wen  : out std_logic;
        io_ren  : out std_logic;
        io_req  : out std_logic;
        io_ack  : in std_logic
        );
end entity;


architecture asynchronous of bf2_io_ctrl is
    
    signal reg_ack  : std_logic := '0';
    signal reg_req  : std_logic := '0';
    signal reg_rst  : std_logic;

    signal reg_cpu_dout : word16;
    
    signal wire_ack : std_logic;
    signal wire_req : std_logic;
    signal wire_rst : std_logic;

begin

    cpu_dout <= reg_cpu_dout when ( cpu_bus = busno ) else (others => 'Z');

    io_dout <= cpu_din;
    io_adr <= cpu_adr;
    
    io_wen <= cpu_wr;
    io_ren <= cpu_rd;
    

    -- The job of this block is to let the CPU talk to some asynchronous IO/IO
    -- on a different clock domain from the CPU's, typically the VGA or other stuff.

    -- A valid IO request coming from the CPU will get registered by the IO
    -- controller, which will put the CPU on a hold until the IO request has
    -- been completed. Once this is done, the CPU is let off of the hold state,
    -- resuming its operation.

    -- An IO request is assumed to be valid when the CPU asserts one of the
    -- cpu_rd or cpu_wr flags and the cpu_bus input equals the parameter of busno.
    -- This is to enable the cpu to talk to a number of different IO buses,
    -- each possible running in a different clock domain etcetera.

    -- When multiple of these IO controllers are used, OR together the cpu_hold
    -- outputs, and connect together the outputs.

    CPU_sync_FSM:
    process(cpu_clk) is        
        type state_t is ( WAIT_CPU_REQUEST, WAIT_IO_READY, WAIT_IO_ACK );
        variable state : state_t := WAIT_CPU_REQUEST;
    begin
        -- CPU-synchronous part
        if( falling_edge( cpu_clk ) ) then
        
            if( cpu_rst = '1' ) then
                state := WAIT_CPU_REQUEST;
                reg_req <= '0';
            else
                case( state ) is
                    when WAIT_CPU_REQUEST =>
                        reg_req <= '0';
                    
                        if( cpu_bus = busno ) then
                            if(( cpu_rd = '1' and cpu_wr = '0' )
                               or ( cpu_rd = '0' and cpu_wr = '1')) then
                                -- Assume a new IO operation was indeed requested
                                cpu_hold <= '1';
                                STATE := WAIT_IO_READY;
                            else
                                cpu_hold <= '0';
                            end if;
                        end if;
                        
                    when WAIT_IO_READY =>
                        -- Wait until the device is ready - lowers its ACK signal.
                        if( reg_ack = '0' ) then
                            -- We're now ready to send it the darn request!                            
                            reg_req <= '1';

                            -- Let's now wait for it to acknowledge our request.
                            state := WAIT_IO_ACK;
                        end if;

                    when WAIT_IO_ACK =>
                        if( reg_ack = '1' ) then
                            -- The device has acknowledged our request, we're
                            -- pretty much done here now.
                            state := WAIT_CPU_REQUEST;

                            -- Register IO->CPU data. This is done 'just to be
                            -- sure' the IO's data does not wander away once we
                            -- lower our request line.
                            reg_cpu_dout <= io_din;

                            -- Since we're done with our request, lower our
                            -- request line as a thank you to the IO.
                            reg_req <= '0';

                            -- The CPU should no longer be held in the hold
                            -- state to enable it to process the possibly read
                            -- IO data and continue execution.
                            cpu_hold <= '0';
                        end if;
                    when others =>
                      state := WAIT_CPU_REQUEST;
                end case;
            end if;
        end if;
    end process;
    
    sync_rst:
    entity work.synchronizer
    port map (
      src_clk => cpu_clk,
      tgt_clk => io_clk,
      req_in  => cpu_rst,
      req_out => io_rst
    );
    
    sync_ack:
    entity work.synchronizer
    port map (
      src_clk => io_clk,
      tgt_clk => cpu_clk,
      req_in  => io_ack,
      req_out => reg_ack
    );
    
    sync_req:
    entity work.synchronizer
    port map (
      src_clk => cpu_clk,
      tgt_clk => io_clk,
      req_in  => reg_req,
      req_out => io_req
    );


end architecture;
