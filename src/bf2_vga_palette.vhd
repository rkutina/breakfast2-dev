library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.BF2_COMMON.ALL;
use WORK.BF2_VGA_COMMON.ALL;

entity bf2_vga_palette is
  port(
    clk : in std_logic;
    
    pal_din   : in  word8;
    pal_adr   : in  word8;
    pal_wen   : in  std_logic;
    pal_dout  : out word8;
    
    pipe_in   : in  vgapipe_r;
    pipe_out  : out vgapipe_r
  );
end bf2_vga_palette;

architecture impl1 of bf2_vga_palette is
  type palram_t is array(0 to 255) of word8;
  
  signal palram : palram_t := (
    "00000000", --K
    "00100101",
    "01101110",
    "11111111",
    
    "11100000", --R
    "01100000", 
    "01000000",
    "00100000", 
    
    "11111100", --Y
    "01101100", 
    "01001000",
    "00100100",
    
    "00011100", --G
    "00001100", 
    "00001000",
    "00000100",
    
    "00011111", --C
    "00001110", 
    "00001010",
    "00000101",
    
    "00000011", --B
    "00000010",
    "00000001",
    "00000000",
    
    "11100011", --M
    "01100010", 
    "01000001",
    "00100001",
    
    "00000000", --W
    "11111100",
    "01001000",
    "01101110",
    
    others => (others => '0')
  );
  
begin
  
  process( clk ) is
  begin
    if( rising_edge(clk) )then
      if(pal_wen = '1') then
        palram(to_integer(unsigned(pal_adr))) <= pal_din;
      end if;
      
      pal_dout <= palram(to_integer(unsigned(pal_adr)));
      
      pipe_out <= pipe_in;
      pipe_out.rgb <= palram(to_integer(unsigned(pipe_in.rgb)));
    end if;
  end process;
end impl1;

