library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package bf2_common is

  subtype word16 is std_logic_vector(15 downto 0);
  subtype word12 is std_logic_vector(11 downto 0);
  subtype word8 is std_logic_vector(7 downto 0);
  subtype word4 is std_logic_vector(3 downto 0);

  subtype uword16 is unsigned(15 downto 0);
  subtype uword12 is unsigned(11 downto 0);

  subtype sword12 is signed(11 downto 0);

  subtype progctr_t is uword16;

  constant m_AND  : word4 := "0000";
  constant m_IOR  : word4 := "0001";
  constant m_ADD  : word4 := "0010";
  constant m_SUB  : word4 := "0011";
  constant m_XOR  : word4 := "0100";
  constant m_SUBR : word4 := "0101";
  constant m_MVL  : word4 := "0110";
  constant m_MVH  : word4 := "0111";
  constant m_REV  : word4 := "1000";
  constant m_1001 : word4 := "1001";
  constant m_MOV  : word4 := "1010";
  constant m_1011 : word4 := "1011";
  constant m_BSEL : word4 := "1100";
  constant m_1101 : word4 := "1101";
  constant m_BSR  : word4 := "1110";
  constant m_BSL  : word4 := "1111";

  constant aRD0      : word4 := "0000";
  constant aRD1      : word4 := "0001";
  constant aRD2      : word4 := "0010";
  constant aRD3      : word4 := "0011";
  constant aWR0      : word4 := "0100";
  constant aWR1      : word4 := "0101";
  constant aWR2      : word4 := "0110";
  constant aWR3      : word4 := "0111";
  constant aRETURN   : word4 := "1100";
  constant aRETIE    : word4 := "1101";
  constant aINTDIS   : word4 := "1110";
  constant aEXTENDED : word4 := "1111";

  constant ex_CALLREG : word4 := "0000";
  constant ex_JMPREG  : word4 := "0001";

  constant r_I0   : word4 := "0000";
  constant r_I1   : word4 := "0001";
  constant r_I2   : word4 := "0010";
  constant r_I3   : word4 := "0011";
  constant r_I4   : word4 := "0100";
  constant r_OFFA : word4 := "0110";
  constant r_OFFB : word4 := "0111";
  constant r_A0   : word4 := "1000";
  constant r_A1   : word4 := "1001";
  constant r_A2   : word4 := "1010";
  constant r_A3   : word4 := "1011";
  constant r_B0   : word4 := "1100";
  constant r_B1   : word4 := "1101";
  constant r_B2   : word4 := "1110";
  constant r_B3   : word4 := "1111";

  constant op_ALU   : word4 := "0000";
  constant op_JMP   : word4 := "0001";
  constant op_CALL  : word4 := "0010";
  constant op_EXTRA : word4 := "0011";
  constant op_JMPR  : word4 := "0100";
  constant op_JMPRE : word4 := "0101";
  constant op_JMPRZ : word4 := "0110";
  constant op_JMPRC : word4 := "0111";
  constant op_ANDI  : word4 := "1000";
  constant op_IORI  : word4 := "1001";
  constant op_ADDI  : word4 := "1010";
  constant op_SUBI  : word4 := "1011";
  constant op_XORI  : word4 := "1100";
  constant op_SUBRI : word4 := "1101";
  constant op_MVLI  : word4 := "1110";
  constant op_MVHI  : word4 := "1111";

end bf2_common;

package body bf2_common is

end bf2_common;
