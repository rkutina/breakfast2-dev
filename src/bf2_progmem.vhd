library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;

entity bf2_progmem is
  generic(
    size : integer range 16 to 65536 := 128
    );
  port(
    clk : in std_logic;
    rst : in std_logic;

    p_adr  : in  progctr_t;
    p_dout : out word16;

    bus_ren  : in  std_logic;
    bus_wen  : in  std_logic;
    bus_req  : in  std_logic;
    bus_ack  : out std_logic;
    bus_adr  : in  word16;
    bus_din  : in  word16;
    bus_dout : out word16
    );
end bf2_progmem;

architecture impl1 of bf2_progmem is

  type pmem_t is array(0 to size-1) of word16;
  signal pmem : pmem_t := (
  
  

    x"E002",
    x"F001",
    
    x"E100",
    x"F100",

    x"E200",
    x"F200",
    
    -- Display something on the VGA screen
    x"E148",                            -- MVLI R1 'H' 
    x"3125",                            -- WR1 R1 (R2)
    x"A201",                            -- ADDI R2 1
    x"E165",                            -- MVLI R1 'e' 
    x"3125",                            -- WR1 R1 (R2)
    x"A201",                            -- ADDI R2 1
    x"E16C",                            -- MVLI R1 'l' 
    x"3125",                            -- WR1 R1 (R2)
    x"A201",                            -- ADDI R2 1
    x"E16C",                            -- MVLI R1 'l' 
    x"3125",                            -- WR1 R1 (R2)
    x"A201",                            -- ADDI R2 1
    x"E16F",                            -- MVLI R1 'o' 
    x"3125",                            -- WR1 R1 (R2)
    x"A201",                            -- ADDI R2 1
    x"E121",                            -- MVLI R1 '!' 
    x"3125",                            -- WR1 R1 (R2)
    x"A201",                            -- ADDI R2 1
    x"E120",                            -- MVLI R1 ' ' 
    x"3125",                            -- WR1 R1 (R2)
    x"A201",                            -- ADDI R2 1
    x"E13A",                            -- MVLI R1 ':' 
    x"3125",                            -- WR1 R1 (R2)
    x"A201",                            -- ADDI R2 1
    x"E133",                            -- MVLI R1 '3' 
    x"3125",                            -- WR1 R1 (R2)    
    
    -- Send something over UART
    x"E154",                            -- MVLI R1 'T'
    x"3104",                            -- WR0 R1 (R0)    
    x"E168",                            -- MVLI R1 'h'
    x"3104",                            -- WR0 R1 (R0)    
    x"E169",                            -- MVLI R1 'i'
    x"3104",                            -- WR0 R1 (R0)    
    x"E173",                            -- MVLI R1 's'
    x"3104",                            -- WR0 R1 (R0)    
    x"E120",                            -- MVLI R1 ' '
    x"3104",                            -- WR0 R1 (R0)    
    x"E177",                            -- MVLI R1 'w'
    x"3104",                            -- WR0 R1 (R0)    
    x"E16F",                            -- MVLI R1 'o'
    x"3104",                            -- WR0 R1 (R0)    
    x"E172",                            -- MVLI R1 'r'
    x"3104",                            -- WR0 R1 (R0)    
    x"E16B",                            -- MVLI R1 'k'
    x"3104",                            -- WR0 R1 (R0)    
    x"E173",                            -- MVLI R1 's'
    x"3104",                            -- WR0 R1 (R0)    
    x"E121",                            -- MVLI R1 '!'
    x"3104",                            -- WR0 R1 (R0)
    x"E120",                            -- MVLI R1 ' '
    x"3104",                            -- WR0 R1 (R0)    
    x"E13A",                            -- MVLI R1 ':'
    x"3104",                            -- WR0 R1 (R0)    
    x"E133",                            -- MVLI R1 '3'
    x"3104",                            -- WR0 R1 (R0)
    x"E10A",                            -- MVLI R1 '\n'
    x"3104",                            -- WR0 R1 (R0)  

    x"0000",                            -- NOP

    x"4000",                            -- JMPR 0

    others => (others => '0')
    );

  signal i_read  : word16;
  signal i_write : word16;
  signal i_adr   : word16;
  signal i_wen   : std_logic := '0';
  signal i_ren   : std_logic := '0';

begin

  dualport_mem :
  process(clk) is
  begin
    if(falling_edge(clk)) then
      -- CPU's progmem port
      p_dout <= pmem(to_integer(p_adr));

      -- Internal port - this is to actually enable proper synthesis as a
      -- dual-port RAM. The synthesizers typically have somewhat tight
      -- requirements in terms of what may eventually synthesize into block
      -- RAMs etc.
      i_read <= pmem(to_integer(unsigned(i_adr)));

      if(i_wen = '1') then
        pmem(to_integer(unsigned(i_adr))) <= i_write;
      end if;
    end if;
  end process;

  bus_interface :
  process(clk) is
    type state_t is (
      READY,
      DELAY,
      WAIT_ACK,
      OUTPUT
      );

    variable state : state_t := READY;
  begin
    if(rst = '1') then
      bus_ack  <= '0';
      bus_dout <= (others => 'Z');
      i_wen    <= '0';
      state    := READY;

    elsif(falling_edge(clk)) then
      case(state) is
        when READY =>
          bus_ack  <= '0';
          bus_dout <= (others => 'Z');
          i_wen    <= '0';

          if(bus_req = '1') then
            i_adr   <= bus_adr;
            i_wen   <= bus_wen;
            i_ren   <= bus_ren;
            i_write <= bus_din;

            state := DELAY;
          end if;
          
        when DELAY =>
          state := OUTPUT;

        when OUTPUT =>
          state := WAIT_ACK;
          if(i_ren = '1') then
            bus_dout <= i_read;
          end if;

        when WAIT_ACK =>
          bus_ack <= '1';

          if(bus_req <= '0') then
            state := READY;
          end if;
      end case;
    end if;
  end process;

end impl1;

