library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;

-- TODO: add synchronizers to the REQ/ACK lines

entity bf2_busmux is
  generic(
    -- The CPUsynchronous flags disable multistage synchronizers for
    -- CPU-synchronous busses to improve performance
    bus0_cpusynchronous : std_logic := '0';
    bus1_cpusynchronous : std_logic := '0';
    bus2_cpusynchronous : std_logic := '0';
    bus3_cpusynchronous : std_logic := '0'
    );
  port(
    cpu_clk  : in  std_logic;
    cpu_bus  : in  std_logic_vector(1 downto 0);
    cpu_req  : in  std_logic;
    cpu_ack  : out std_logic;
    cpu_din  : out word16;
    cpu_dout : in  word16;
    cpu_adr  : in  word16;
    cpu_wen  : in  std_logic;
    cpu_ren  : in  std_logic;

    bus0_clk  : in  std_logic;
    bus0_req  : out std_logic;
    bus0_ack  : in  std_logic;
    bus0_din  : in  word16;
    bus0_dout : out word16;
    bus0_adr  : out word16;
    bus0_wen  : out std_logic;
    bus0_ren  : out std_logic;

    bus1_clk  : in  std_logic;
    bus1_req  : out std_logic;
    bus1_ack  : in  std_logic;
    bus1_din  : in  word16;
    bus1_dout : out word16;
    bus1_adr  : out word16;
    bus1_wen  : out std_logic;
    bus1_ren  : out std_logic;

    bus2_clk  : in  std_logic;
    bus2_req  : out std_logic;
    bus2_ack  : in  std_logic;
    bus2_din  : in  word16;
    bus2_dout : out word16;
    bus2_adr  : out word16;
    bus2_wen  : out std_logic;
    bus2_ren  : out std_logic;

    bus3_clk  : in  std_logic;
    bus3_req  : out std_logic;
    bus3_ack  : in  std_logic;
    bus3_din  : in  word16;
    bus3_dout : out word16;
    bus3_adr  : out word16;
    bus3_wen  : out std_logic;
    bus3_ren  : out std_logic

    );
end entity;

architecture impl1 of bf2_busmux is
  signal r_bus0_req : std_logic := '0';
  signal r_bus1_req : std_logic := '0';
  signal r_bus2_req : std_logic := '0';
  signal r_bus3_req : std_logic := '0';

  signal r_bus0_ack : std_logic := '0';
  signal r_bus1_ack : std_logic := '0';
  signal r_bus2_ack : std_logic := '0';
  signal r_bus3_ack : std_logic := '0';

begin

  -- Address mux
  process(cpu_bus, cpu_adr) is
  begin
    -- Defaults
    bus0_adr <= (others => '-');
    bus1_adr <= (others => '-');
    bus2_adr <= (others => '-');
    bus3_adr <= (others => '-');

    -- Mux
    case(cpu_bus) is
      when "00"   => bus0_adr <= cpu_adr;
      when "01"   => bus1_adr <= cpu_adr;
      when "10"   => bus2_adr <= cpu_adr;
      when "11"   => bus3_adr <= cpu_adr;
      when others => null;  -- Defaults have already been caught above
    end case;
  end process;

  -- Data out mux
  process(cpu_bus, cpu_dout) is
  begin
    bus0_dout <= (others => '-');
    bus1_dout <= (others => '-');
    bus2_dout <= (others => '-');
    bus3_dout <= (others => '-');

    case(cpu_bus) is
      when "00"   => bus0_dout <= cpu_dout;
      when "01"   => bus1_dout <= cpu_dout;
      when "10"   => bus2_dout <= cpu_dout;
      when "11"   => bus3_dout <= cpu_dout;
      when others => null;  -- Defaults have already been caught above
    end case;
  end process;

  -- Data in mux
  with cpu_bus select cpu_din <=
    bus0_din        when "00",
    bus1_din        when "01",
    bus2_din        when "10",
    bus3_din        when "11",
    (others => 'Z') when others;

  -- WEN, REN, REQ mux
  process(cpu_bus, cpu_wen, cpu_ren, cpu_req) is
  begin
    bus0_wen <= '0'; bus0_ren <= '0'; r_bus0_req <= '0';
    bus1_wen <= '0'; bus1_ren <= '0'; r_bus1_req <= '0';
    bus2_wen <= '0'; bus2_ren <= '0'; r_bus2_req <= '0';
    bus3_wen <= '0'; bus3_ren <= '0'; r_bus3_req <= '0';

    case(cpu_bus) is
      when "00"   => bus0_wen <= cpu_wen; bus0_ren <= cpu_ren; r_bus0_req <= cpu_req;
      when "01"   => bus1_wen <= cpu_wen; bus1_ren <= cpu_ren; r_bus1_req <= cpu_req;
      when "10"   => bus2_wen <= cpu_wen; bus2_ren <= cpu_ren; r_bus2_req <= cpu_req;
      when "11"   => bus3_wen <= cpu_wen; bus3_ren <= cpu_ren; r_bus3_req <= cpu_req;
      when others => null;
    end case;
  end process;

  -- ACK mux
  with cpu_bus select cpu_ack <=
    r_bus0_ack when "00",
    r_bus1_ack when "01",
    r_bus2_ack when "10",
    r_bus3_ack when "11",
    '0'        when others;


  sync0_ack : entity work.synchronizer(impl1)
    generic map(
      thru => bus0_cpusynchronous
      )
    port map(
      tgt_clk => cpu_clk,
      req_in  => bus0_ack,
      req_out => r_bus0_ack
      );
  sync1_ack : entity work.synchronizer(impl1)
    generic map(
      thru => bus1_cpusynchronous
      )
    port map(
      tgt_clk => cpu_clk,
      req_in  => bus1_ack,
      req_out => r_bus1_ack
      );
  sync2_ack : entity work.synchronizer(impl1)
    generic map(
      thru => bus2_cpusynchronous
      )
    port map(
      tgt_clk => cpu_clk,
      req_in  => bus2_ack,
      req_out => r_bus2_ack
      );
  sync3_ack : entity work.synchronizer(impl1)
    generic map(
      thru => bus3_cpusynchronous
      )
    port map(
      tgt_clk => cpu_clk,
      req_in  => bus3_ack,
      req_out => r_bus3_ack
      );


  sync0_req : entity work.synchronizer(impl1)
    generic map(
      thru => bus0_cpusynchronous
      )
    port map(
      tgt_clk => bus0_clk,
      req_in  => r_bus0_req,
      req_out => bus0_req
      );
  sync1_req : entity work.synchronizer(impl1)
    generic map(
      thru => bus1_cpusynchronous
      )
    port map(
      tgt_clk => bus1_clk,
      req_in  => r_bus1_req,
      req_out => bus1_req
      );
  sync2_req : entity work.synchronizer(impl1)
    generic map(
      thru => bus2_cpusynchronous
      )
    port map(
      tgt_clk => bus2_clk,
      req_in  => r_bus2_req,
      req_out => bus2_req
      );
  sync3_req : entity work.synchronizer(impl1)
    generic map(
      thru => bus3_cpusynchronous
      )
    port map(
      tgt_clk => bus3_clk,
      req_in  => r_bus3_req,
      req_out => bus3_req
      );

end architecture;
