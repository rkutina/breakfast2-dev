library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;

entity bf2_regfile is
  generic(
    rising    : boolean                := false;
    iram_size : integer range 4 to 256 := 64
    );
  port(
    clk : in std_logic;
    rst : in std_logic;
    ena : in std_logic;

    adrW : in word4;
    adrA : in word4;
    adrB : in word4;


    ren : in std_logic;
    wen : in std_logic;
    wpc : in std_logic;

    datW : in  word16;
    datA : out word16;
    datB : out word16;

    pc_in  : in  word16;
    pc_out : out word16
    );
end bf2_regfile;

architecture impl1 of bf2_regfile is

  signal iram_adrW : unsigned(15 downto 0);
  signal iram_adrA : unsigned(15 downto 0);
  signal iram_adrB : unsigned(15 downto 0);

  signal iram_offW : unsigned(15 downto 0);
  signal iram_offA : unsigned(15 downto 0);
  signal iram_offB : unsigned(15 downto 0);


  type iram_t is array(0 to iram_size-1) of word16;
  type indgpr_t is array(0 to 3) of word16;  -- independent GPR  
  type intreg_t is array(0 to 3) of word16;  -- 'internal' registers

  signal iram   : iram_t   := (others => (others => '0'));
  signal igpr   : indgpr_t := (others => (others => '0'));
  signal intreg : intreg_t := (others => (others => '0'));

  alias r_pc      : word16 is intreg(1);
  alias r_iramp_a : word16 is intreg(2);
  alias r_iramp_b : word16 is intreg(3);

begin

  --- Assign PC output
  pc_out <= r_pc;

  ---
  --- IRAM offset calc
  ---
  iram_offW <= unsigned(r_iramp_a) when adrW(2) = '0' else unsigned(r_iramp_b);
  iram_offA <= unsigned(r_iramp_a) when adrA(2) = '0' else unsigned(r_iramp_b);
  iram_offB <= unsigned(r_iramp_a) when adrB(2) = '0' else unsigned(r_iramp_b);

  iram_adrW <= iram_offW + resize(unsigned(adrW(1 downto 0)), 16);
  iram_adrA <= iram_offA + resize(unsigned(adrA(1 downto 0)), 16);
  iram_adrB <= iram_offB + resize(unsigned(adrB(1 downto 0)), 16);

  -- This was written in one process, just so it is
  -- easier to make to output the just written value

  register_write :
  process(clk, rst) is
  begin

    if(rst = '1') then
      r_pc      <= (others => '0');
      r_iramp_a <= (others => '0');
      r_iramp_b <= (others => '0');
    --r_iramp_a <= word16(to_unsigned(iram_size-1, 16));
    --r_iramp_b <= word16(to_unsigned(iram_size-1, 16));
    else

      if((not rising and falling_edge(clk)) or (rising and rising_edge(clk))) then
        if(wpc = '1' and ena = '1') then
          r_pc <= pc_in;  -- default new PC value, can be overwritten with a regular write, as expected
        end if;

        --- Register write
        if(wen = '1' and ena = '1') then
          if (adrW(3) = '1') then
            iram(to_integer(iram_adrW)) <= datW;

          elsif (adrW(3 downto 2) = "00") then
            igpr(to_integer(unsigned(adrW(1 downto 0)))) <= datW;

          elsif (adrW(3 downto 2) = "01") then
            intreg(to_integer(unsigned(adrW(1 downto 0)))) <= datW;
          else
          -- wat
          end if;

        end if;

      end if;
    end if;
  end process;

  register_read :
  process(clk) is
  begin
    if((not rising and falling_edge(clk)) or (rising and rising_edge(clk))) then
      if(ren = '1' and ena = '1') then

        if (adrA(3) = '1') then
          datA <= iram(to_integer(iram_adrA));

        elsif (adrA(3 downto 2) = "00") then
          datA <= igpr(to_integer(unsigned(adrA(1 downto 0))));

        elsif (adrA(3 downto 2) = "01") then
          datA <= intreg(to_integer(unsigned(adrA(1 downto 0))));

        else
        -- wat
        end if;

        if (adrB(3) = '1') then
          datB <= iram(to_integer(iram_adrB));

        elsif (adrB(3 downto 2) = "00") then
          datB <= igpr(to_integer(unsigned(adrB(1 downto 0))));

        elsif (adrB(3 downto 2) = "01") then
          datB <= intreg(to_integer(unsigned(adrB(1 downto 0))));

        else
        -- wat
        end if;
      end if;
    end if;
  end process;
end impl1;
