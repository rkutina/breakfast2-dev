library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use WORK.BF2_COMMON.all;
use WORK.BF2_VGA_COMMON.all;

entity bf2_vga_chargen is
  generic(
    pixel_doubling : integer := 0
    );
  port(
    clk : in std_logic;
    rst : in std_logic;

    --- Character generator ROM connections
    charmem_d : in  word16;
    charmem_a : out std_logic_vector(10 downto 0);

    --- VRAM connections
    vram_d : in  word16;
    vram_a : out std_logic_vector(13-2*pixel_doubling downto 0);

    --- VGA generation pipeline connections
    pipe_in  : in  vgapipe_r;
    pipe_out : out vgapipe_r
    );
end bf2_vga_chargen;

architecture impl1 of bf2_vga_chargen is

  signal pipereg0 : vgapipe_r;
  signal pipereg1 : vgapipe_r;
begin

  vmem_adrgen : process(clk) is
    variable u_hpix : unsigned(9 downto 0);
    variable u_vpix : unsigned(9 downto 0);
  begin
    if(falling_edge(clk)) then
      u_hpix := to_unsigned(pipe_in.hpix, 10);
      u_vpix := to_unsigned(pipe_in.vpix, 10);

      -- vram_a <= (std_logic_vector(u_vpix(9 downto 3)) & std_logic_vector(u_hpix(9 downto 3)));
      
      vram_a <= (std_logic_vector(u_vpix(9 downto 3+pixel_doubling))
                 & std_logic_vector(u_hpix(9 downto 3+pixel_doubling)));
    end if;
  end process;

  charmem_adrgen : process(clk) is
    variable u_vpix : unsigned(9 downto 0);
  begin
    if(falling_edge(clk)) then
      u_vpix := to_unsigned(pipereg0.vpix, 10) srl pixel_doubling;

      charmem_a <= (vram_d(7 downto 4) & std_logic_vector(u_vpix(2 downto 0)) & vram_d(3 downto 0));
    end if;
  end process;

  pipeline_0 : process(clk) is
  begin
    if(rising_edge(clk)) then
      pipereg0 <= pipe_in;
    end if;
  end process;

  pipeline_1 : process(clk) is
  begin
    if(rising_edge(clk)) then
      pipereg1     <= pipereg0;
      pipereg1.rgb <= vram_d(15 downto 8);
    end if;
  end process;

  output_stuff : process(clk) is
    variable u_hpix  : unsigned(9 downto 0);
    variable colbits : unsigned(15 downto 0);
  begin
    if(rising_edge(clk)) then
      u_hpix := to_unsigned(pipereg1.hpix, 10) srl pixel_doubling;

      pipe_out <= pipereg1;

      colbits := unsigned(charmem_d) srl 14-to_integer(u_hpix(2 downto 0) & "0");

      pipe_out.rgb <= (pipereg1.rgb(7 downto 2)
                       & std_logic_vector(colbits(1 downto 0)));

    end if;
  end process;

end impl1;

