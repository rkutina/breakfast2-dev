library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity uart_simple is
  generic(
    adr_reg_rx : std_logic_vector(15 downto 0) := x"0101";
    adr_reg_tx : std_logic_vector(15 downto 0) := x"0102";
    divide     : integer                       := 500*32  -- Assuming 19.2MHz clock, this will result in 1200 bd operation
    );
  port(
    clk_16baud : in std_logic;
    rst        : in std_logic;

    rx : in  std_logic;
    tx : out std_logic;

    io_req  : in  std_logic;
    io_ack  : out std_logic;
    io_ren  : in  std_logic;
    io_wen  : in  std_logic;
    io_adr  : in  std_logic_vector(15 downto 0);
    io_din  : in  std_logic_vector(15 downto 0);
    io_dout : out std_logic_vector(15 downto 0);

    rx_dataready : out std_logic;
    tx_ready     : out std_logic
    );
end entity;

architecture impl1 of uart_simple is

  type buf_t is array(0 to 15) of std_logic_vector(7 downto 0);
  signal buf_tx : buf_t;
  signal buf_rx : buf_t;

  subtype bufptr_t is integer range 0 to 15;
  signal ptr_txbuf_wr : bufptr_t;
  signal ptr_txbuf_rd : bufptr_t;
  signal ptr_rxbuf_wr : bufptr_t;
  signal ptr_rxbuf_rd : bufptr_t;

  signal shifter_rx : unsigned(7 downto 0);
  signal shifter_tx : unsigned(7 downto 0);

  signal clk : std_logic;

  signal flag_rx_dataready : std_logic;
  signal flag_tx_ready     : std_logic;

begin
  rx_dataready <= flag_rx_dataready;
  tx_ready     <= flag_tx_ready;

  flag_tx_ready <= '1' when (((ptr_txbuf_wr+1) mod 16 /= ptr_txbuf_rd)) else '0';

  flag_rx_dataready <= '1' when (ptr_rxbuf_wr /= ptr_rxbuf_rd) else '0';

  divider :
  process(clk_16baud, rst) is
    variable divcnt : integer;
  begin
    if(rst = '1') then
      divcnt := 0;
      clk    <= '0';
    elsif(rising_edge(clk_16baud))then
      if(divcnt >= divide/2) then
        divcnt := 0;
        clk    <= not clk;
      else
        divcnt := divcnt + 1;
      end if;
    end if;
  end process;

  io_interface :
  process(rst, clk_16baud) is
    type state_t is (
      WAIT_REQUEST,
      PROCESS_REQUEST,
      WAIT_ACK
      );

    variable state : state_t := WAIT_REQUEST;
  begin

    if(rst = '1') then
      state        := WAIT_REQUEST;
      io_ack       <= '0';
      io_dout      <= (others => 'Z');
      ptr_rxbuf_rd <= 0;
      ptr_txbuf_wr <= 0;
    elsif(rising_edge(clk_16baud)) then

      case(state) is
        when WAIT_REQUEST =>
          io_ack  <= '0';
          io_dout <= (others => 'Z');

          if(io_req = '1') then
            state := PROCESS_REQUEST;
          end if;

        when PROCESS_REQUEST =>

          case(io_adr) is
            when adr_reg_tx =>
              if(io_wen = '1' and flag_tx_ready = '1') then
                buf_tx(ptr_txbuf_wr) <= io_din(7 downto 0);
                ptr_txbuf_wr         <= ptr_txbuf_wr + 1;
                io_dout              <= (others => 'Z');
                state                := WAIT_ACK;
              else
                io_dout <= (others => 'Z');
                state   := WAIT_REQUEST;
              end if;

            when adr_reg_rx =>
              if(io_ren = '1' and flag_rx_dataready = '1') then
                io_dout(7 downto 0)  <= buf_rx(ptr_rxbuf_rd);
                io_dout(15 downto 8) <= (others => '0');
                ptr_rxbuf_rd         <= ptr_rxbuf_rd + 1;
                state                := WAIT_ACK;
              else
                io_dout <= (others => 'Z');
                state := WAIT_REQUEST;
              end if;

            when others =>
              io_dout <= (others => 'Z');
              state   := WAIT_ACK;
          end case;

        when WAIT_ACK =>
          io_ack    <= '1';
          if(io_req <= '0') then
            state := WAIT_REQUEST;
          end if;

      end case;

    end if;

  end process;

  receiver :
  process(clk) is
    variable delaycounter : integer range 0 to 15;

    variable can_rx     : std_logic;
    variable receiving  : std_logic;
    variable bitcounter : integer range 0 to 9;
  begin

    if(rst = '1') then
      ptr_rxbuf_wr <= 0;
      receiving    := '0';

    elsif(rising_edge(clk)) then
      if(ptr_rxbuf_rd /= ptr_rxbuf_wr) then
        if(receiving = '0') then
          if(rx = '0') then
            -- Start receiving
            -- Offset clock by half a bitwidth
            delaycounter := 8;  --8, because the actual clock here is 16/bitwidth
            receiving    := '1';
            bitcounter   := 0;
          end if;
        end if;
      end if;

      if(receiving = '1') then
        if(delaycounter /= 15) then
          delaycounter := delaycounter + 1;
        else
          delaycounter := 0;
        end if;

        if(delaycounter = 0) then
          case(bitcounter) is
            when 0 =>
              shifter_rx <= (others => '0');
              bitcounter := bitcounter + 1;
            when 9 =>
              if(ptr_rxbuf_wr /= 15)then
                ptr_rxbuf_wr <= ptr_rxbuf_wr + 1;
              else
                ptr_rxbuf_wr <= 0;
              end if;
              buf_rx(ptr_rxbuf_wr) <= std_logic_vector(shifter_rx);
              receiving            := '0';
              bitcounter           := 0;

            when others =>
              bitcounter    := bitcounter + 1;
              shifter_rx    <= (shifter_rx srl 1);
              shifter_rx(7) <= rx;
          end case;
        end if;
      end if;
    end if;

  end process;


  transmitter :
  process(clk, rst) is
    variable bitcounter : integer range 0 to 9;

    variable transmitting : std_logic;

    variable delaycounter : integer range 0 to 15;
  begin
    if(rst = '1') then
      bitcounter   := 0;
      ptr_txbuf_rd <= 0;
      transmitting := '0';
      tx           <= '1';              --!

    elsif(rising_edge(clk)) then

      if(ptr_txbuf_rd /= ptr_txbuf_wr) then
        if(transmitting = '0') then
          transmitting := '1';
          bitcounter   := 0;
          delaycounter := 0;
        end if;
      end if;

      if(delaycounter /= 15) then
        delaycounter := delaycounter + 1;
      else
        delaycounter := 0;
      end if;

      if(transmitting = '1') then
        if(delaycounter = 0) then
          case(bitcounter) is
            when 0 =>
              tx         <= '0';        -- start bit
              bitcounter := bitcounter + 1;
              shifter_tx <= unsigned(buf_tx(ptr_txbuf_rd));

            when 9 =>
              tx           <= '1';      -- stop bit
              transmitting := '0';
              bitcounter   := 0;

              if(ptr_txbuf_rd /= 15)then
                ptr_txbuf_rd <= ptr_txbuf_rd + 1;
              else
                ptr_txbuf_rd <= 0;
              end if;

            when others =>
              tx         <= shifter_tx(0);
              shifter_tx <= shifter_tx srl 1;
              bitcounter := bitcounter + 1;
          end case;
        end if;
      end if;
    end if;
  end process;

end architecture;
