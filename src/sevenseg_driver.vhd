library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity sevenseg_driver is
    generic(
        ndigits : integer := 4
        );
    port(
        clk : in std_logic;
        rst : in std_logic;

        data_in : in std_logic_vector((ndigits*4)-1 downto 0);

        commons  : out std_logic_vector(ndigits-1 downto 0);
        segments : out std_logic_vector(6 downto 0)
        );
end sevenseg_driver;

architecture impl1 of sevenseg_driver is

    type pattern_t is array(0 to 15) of std_logic_vector(6 downto 0);
    constant pattern : pattern_t := (
        -- aaa
        --f   b
        -- ggg
        --e   c
        -- ddd

        --gfedcba
        "0111111",                      -- 0
        "0000110",                      -- 1
        "1011011",                      -- 2
        "1001111",                      -- 3
        "1100110",                      -- 4
        "1101101",                      -- 5
        "1111101",                      -- 6
        "0000111",                      -- 7
        "1111111",                      -- 8
        "1101111",                      -- 9
        "1110111",                      -- A
        "1111100",                      -- b
        "0111001",                      -- C
        "1011110",                      -- d
        "1111001",                      -- E
        "1110001",                      -- F
        others => (others =>'-')
        );

    signal blank        : std_logic;
    signal commons_reg  : std_logic_vector(ndigits-1 downto 0);
    signal segments_reg : std_logic_vector(6 downto 0);

    signal data_reg : std_logic_vector((ndigits*4)-1 downto 0);

    subtype waitctr_t is unsigned(11 downto 0);
    signal waitctr    : waitctr_t;
begin

    commons  <= (not commons_reg)  when blank = '0' else (others => '1');
    segments <= (not segments_reg) when blank = '0' else (others => '1');

    magic :
    process(clk, rst) is
        type state_t is (
            REG_INPUT,
            DISPLAY,
            NEXT_DIGIT
            );
        variable state : state_t;
    begin

        if(rst = '1') then
            state       := REG_INPUT;
            blank       <= '1';
            waitctr     <= (others => '0');
            commons_reg <= (0      => '1', others => '0');
        elsif(rising_edge(clk)) then
            case (state) is
                when REG_INPUT =>
                    data_reg <= data_in;
                    state    := DISPLAY;
                    blank <= '1';                    
                when DISPLAY =>
                    segments_reg <= pattern(to_integer(unsigned(data_reg(3 downto 0))));
                    blank <= '0';
                
                    if(waitctr = waitctr_t'(others => '1')) then
                        state := NEXT_DIGIT;
                    end if;

                    waitctr <= waitctr + 1;

                when NEXT_DIGIT =>
                    blank <= '1';

                    data_reg <= std_logic_vector(unsigned(data_reg) srl 4);
                    commons_reg <= std_logic_vector(unsigned(commons_reg) rol 1);
                    
                    if(commons_reg(ndigits-1) = '1') then
                        state := REG_INPUT;
                    else
                        state := DISPLAY;
                    end if;

                when others =>
                    state := REG_INPUT;
            end case;
        end if;

    end process;

end impl1;

