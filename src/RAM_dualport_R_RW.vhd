library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity RAM_dualport_R_RW is
    generic(
        datwidth  : integer := 16;
        adrwidth  : integer := 16;
        size      : integer := 1024
        );
    port(
        clk : in std_logic;
        
        p0_adr  : in  std_logic_vector(adrwidth-1 downto 0);
        p0_dout : out std_logic_vector(datwidth-1 downto 0);
        
        p1_adr  : in  std_logic_vector(adrwidth-1 downto 0);
        p1_din  : in  std_logic_vector(datwidth-1 downto 0);
        p1_dout : out std_logic_vector(datwidth-1 downto 0);
        p1_wen  : in  std_logic
        );
end RAM_dualport_R_RW;

architecture impl1 of RAM_dualport_R_RW is

    type ram_t is array(0 to size-1) of std_logic_vector(datwidth-1 downto 0);
    signal ram : ram_t := (others => (others => '0'));

begin

    process( clk ) is
    begin
        if( rising_edge(clk) ) then
            
            if(p1_wen = '1') then
                ram(to_integer(unsigned(p1_adr))) <= p1_din;
            end if;
            
            p0_dout <= ram(to_integer(unsigned(p0_adr)));
            p1_dout <= ram(to_integer(unsigned(p1_adr)));
            
        end if;
    end process;

end impl1;
