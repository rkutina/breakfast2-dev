LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY vga_top_tb IS
END vga_top_tb;
 
ARCHITECTURE behavior OF vga_top_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bf2_vga_top
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         io_ena : IN  std_logic;
         io_req : IN  std_logic;
         io_ack : OUT  std_logic;
         io_adr : IN  std_logic_vector(15 downto 0);
         io_in : IN  std_logic_vector(15 downto 0);
         io_out : OUT  std_logic_vector(15 downto 0);
         rgb : OUT  std_logic_vector(7 downto 0);
         hsync : OUT  std_logic;
         vsync : OUT  std_logic;
         blank : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal io_req : std_logic := '0';
   signal io_adr : std_logic_vector(15 downto 0) := (others => '0');
   signal io_in : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal io_ack : std_logic;
   signal io_out : std_logic_vector(15 downto 0);
   signal rgb : std_logic_vector(7 downto 0);
   signal hsync : std_logic;
   signal vsync : std_logic;
   signal blank : std_logic;

   -- Clock period definitions
   constant clk_period : time := 15.63 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bf2_vga_top PORT MAP (
          clk => clk,
          rst => rst,
          io_req => io_req,
          io_ack => io_ack,
          io_adr => io_adr,
          io_in => io_in,
          io_out => io_out,
          rgb => rgb,
          hsync => hsync,
          vsync => vsync,
          blank => blank
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.


      -- insert stimulus here 

      wait;
   end process;

END;
