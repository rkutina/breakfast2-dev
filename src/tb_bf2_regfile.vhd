LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY tb_bf2_regfile IS
END tb_bf2_regfile;
 
ARCHITECTURE behavior OF tb_bf2_regfile IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bf2_regfile
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         adrW : IN  std_logic_vector(3 downto 0);
         adrA : IN  std_logic_vector(3 downto 0);
         adrB : IN  std_logic_vector(3 downto 0);
         ren : IN  std_logic;
         wen : IN  std_logic;
         wpc : IN  std_logic;
         datW : IN  std_logic_vector(15 downto 0);
         datA : OUT  std_logic_vector(15 downto 0);
         datB : OUT  std_logic_vector(15 downto 0);
         pc_in : IN  std_logic_vector(15 downto 0);
         pc_out : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal adrW : std_logic_vector(3 downto 0) := (others => '0');
   signal adrA : std_logic_vector(3 downto 0) := (others => '0');
   signal adrB : std_logic_vector(3 downto 0) := (others => '0');
   signal ren : std_logic := '0';
   signal wen : std_logic := '0';
   signal wpc : std_logic := '0';
   signal datW : std_logic_vector(15 downto 0) := (others => '0');
   signal pc_in : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal datA : std_logic_vector(15 downto 0);
   signal datB : std_logic_vector(15 downto 0);
   signal pc_out : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bf2_regfile PORT MAP (
          clk => clk,
          rst => rst,
          adrW => adrW,
          adrA => adrA,
          adrB => adrB,
          ren => ren,
          wen => wen,
          wpc => wpc,
          datW => datW,
          datA => datA,
          datB => datB,
          pc_in => pc_in,
          pc_out => pc_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      rst <= '1';
      wait for 100 ns;	
      rst <= '0';

      adrW <= "1110";
      datW <= "0000000000000100";
      wen <= '1';
      wait for clk_period*1;
      
      wen <= '1';
      adrW <= "0010";
      datW <= "1111000011110000";
      wait for clk_period*1;
      
      wen <= '1';
      adrW <= "0011";
      datW <= "1111000011110001";
      wait for clk_period*1;
      
      adrW <= "1110";
      datW <= "0000000000000110";
      wen <= '1';
      wait for clk_period*1;
      
      wen <= '1';
      adrW <= "0011";
      datW <= "1111000011110010";
      wait for clk_period*1;
      
      wait;
   end process;

END;
