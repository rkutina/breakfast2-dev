library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity RAM_generic is
    generic(
        datwidth  : integer := 16;
        adrwidth  : integer := 16;
        size      : integer := 1024
        );
    port(
        clk : in std_logic;
        
        p0_adr  : in  std_logic_vector(adrwidth-1 downto 0);
        p0_din  : in  std_logic_vector(datwidth-1 downto 0);
        p0_dout : out std_logic_vector(datwidth-1 downto 0);
        p0_wen  : in  std_logic
        );
end RAM_generic;

architecture impl1 of RAM_generic is

    type ram_t is array(0 to size-1) of std_logic_vector(datwidth-1 downto 0);
    signal ram : ram_t := (others => (others => '0'));

begin

    process( clk ) is
    begin
        if( rising_edge(clk) ) then
            if(p0_wen = '1') then
                ram(to_integer(unsigned(p0_adr)) mod size) <= p0_din;
            end if;
        end if;
    end process;

    process( clk ) is
    begin
        if( rising_edge(clk) ) then
            p0_dout <= ram(to_integer(unsigned(p0_adr)) mod size);
        end if;
    end process;
    
end impl1;
